unit icalClass;



interface

uses ExtCtrls, Windows, Grids, sysUtils, dialogs,
      Graphics;

type
  TRGBTripleArray = array[0..400] of TRGBTriple;
  PRGBTripleArray = ^TRGBTripleArray;
  T3x3FloatArray  = array[0..2] of array[0..2] of Extended;
  TVector         = array[0..2] of Extended;

type tkordinatimage=record
   posisi:array[0..9999]of tpoint;
   luas:integer;
   R,Ed,Ao : double;
  end;


type TIcal=Class
  mImage : TBitmap;
  Mask : T3x3FloatArray;
  MaskX, MaskY : T3x3FloatArray;
  posx,posy:integer;
  warnaSkr:tcolor;
  arrayKordinatImage:array[0..100]of tkordinatimage;
  jumArrayKI:integer;
  Ao,R,Ed:Extended;
  constructor Create;
  destructor free;
  function msa(const nInputPoint,iterasi,radius:integer;const img:timage):Tbitmap;
  function msa3(const nInputPoint,iterasi,radius:integer;const img:timage):Tbitmap;

  function msaGrey(const iterasi,nInputPoint,radius:integer;const img:Timage):TBitmap;
  function ImgConvertToGrayscale:Tbitmap;
  function RgbToGray (Clr:TColor) : byte;
  function ColorDetection(const cl1,cl2: TColor;tolerance:real):boolean;
  function SobelEdge(ABias : integer;const img:Timage) : TBitmap;
  function lokalisasi(ToleransiLokalisasi,minpixelsvalue:Integer;const img:timage):TBitmap;
  procedure countRecursive(var image13: TImage;
                         const curcolor:TColor;
                         const X,Y,Tolerance : integer;
                         var area : tkordinatimage);
  function binarisasi(casal:TBitmap;top: integer):tbitmap;
  function analisaBentuk(sourceimg:timage;warna:tcolor;amer:double;index:integer):tBitmap;
  function drawChar(sourceimg:timage;warna:tcolor;index:integer):tBitmap;

  Procedure PanalisaBentuk(sourceimg: timage; warna: tcolor; amer: double;index:integer);
  function MClassification(x_uji: TVector; statistik,covariance:T3x3FloatArray):boolean;
  function SobelEdge2(Sg1,Sg2:TstringGrid;ABias : integer;const img:Timage) : TBitmap;

end;




const
  PercentR = 0.299;
  PercentG = 0.587;
  PercentB = 0.114;

function nilaivector(titik1,titik2:tpoint):double;
procedure splitRGB(const cl1:TColor; var r,g,b : byte);
function invert(Casal:Tbitmap):tbitmap;

implementation


{ TIcal }

function TIcal.ImgConvertToGrayscale:Tbitmap;
var x,y:integer;
Clr:TColor; ClrGray:byte;
row1,row2:PRGBTripleArray;
temp:TBitmap;
begin
temp:=TBitmap.Create;
temp.Width:=mimage.Width;
temp.Height:=mimage.Height;
temp.PixelFormat:=pf24bit;

  for x:=0 to MImage.Height-1 do
  begin
  row1:=Mimage.ScanLine[x];
  row2:=temp.ScanLine[x];
    for y:=0 to MImage.Width-1 do
    begin
      Clr := rgb(row1[y].rgbtRed,row1[y].rgbtGreen,row1[y].rgbtBlue);
      ClrGray := RgbToGray (Clr);
      row2[y].rgbtBlue := ClrGray;
      row2[y].rgbtRed := ClrGray;
      row2[y].rgbtGreen := ClrGray;
    end;
  end;
result := temp;
end;

function TIcal.RgbToGray (Clr:TColor) : byte;
var r,g,b:byte;
begin
  r := GetRValue(Clr);
  g := GetGValue(Clr);
  b := GetBValue(Clr);
  Result := Round (r*PercentR + g*PercentG + b*PercentB);
end;


constructor TIcal.Create;
begin
  mimage:=TBitmap.Create;
  mImage.PixelFormat:=pf24bit;
  jumArrayKI:=0;
end;

function TIcal.msa(const nInputPoint,iterasi,Radius: integer; const img: timage): TBitmap;
var
  temp2,temp1:tbitmap;
  inputPointColor  : TColor;
  inputPointRed, inputPointGreen, inputPointBlue : integer;
  currentPixelRedDistance, currentPixelBlueDistance, currentPixelGreenDistance : integer;
  currentPixelColorR,currentPixelColorG,currentPixelColorB:byte;
  i,j,a,b,c,d,e : integer;
  accumulatedRedValues,accumulatedGreenValues,accumulatedBlueValues : double;
  totalClusterPixels : integer;
  row1,row2:PRGBTripleArray;
begin
temp1:=img.Picture.Bitmap;
temp2:=tbitmap.create;
temp2.Width:=temp1.Width;
temp2.Height:=temp1.Height;
temp2.PixelFormat:=pf24bit;
mImage:=img.Picture.Bitmap;

for i:=0 to temp1.Height-1 do
  begin
  row1:=Mimage.ScanLine[i];
  row2:=temp2.ScanLine[i];
    for j:=0 to MImage.Width-1 do
      begin
      row2[j].rgbtRed:=row1[j].rgbtRed;
      row2[j].rgbtGreen:=row1[j].rgbtGreen;
      row2[j].rgbtBlue:=row1[j].rgbtBlue;
      end;
  end;



   // for making windows input points
for e := 0 to nInputPoint-1 do
   for c := 0 to nInputPoint-1 do
   begin

    inputPointColor := TColor(img.Canvas.Pixels[round(e*temp1.width/(nInputPoint+1)),round(c*temp1.height/(nInputPoint)+1)]);
    // given input point updated and processed prop.numberOfIterations times
    for d := 1 to iterasi do
    begin

    inputPointRed := GetRValue(inputPointColor);
    inputPointGreen := GetGValue(inputPointColor);
    inputPointBlue := GetBValue(inputPointColor);

    accumulatedRedValues := 0;
    accumulatedGreenValues := 0;
    accumulatedBlueValues := 0;
    totalClusterPixels := 0;

    // for travel through image pixels
    for a := 0 to temp1.height-1 do
    begin
    row1:=temp1.ScanLine[a];
    row2:=temp2.ScanLine[a];

      for b := 0 to temp1.width-1 do
      begin

        //---------------------------------------
        currentPixelColorR :=  row1[b].rgbtRed;
        currentPixelColorG :=  row1[b].rgbtGreen;
        currentPixelColorB :=  row1[b].rgbtBlue;

        currentPixelRedDistance := currentPixelColorR - inputPointRed;
        currentPixelGreenDistance := currentPixelColorG - inputPointGreen;
        currentPixelBlueDistance := currentPixelColorB - inputPointBlue;

        // calculate euclidian distance (it's square root to be taken, but square values mathches here)
        if (((currentPixelRedDistance * currentPixelRedDistance) +
             (currentPixelGreenDistance * currentPixelGreenDistance) +
             (currentPixelBlueDistance * currentPixelBlueDistance)) < Radius) then //1000 konstanta, bisa di-ubah2
        begin


        // totalClusterPixels hold how many pixels belong to a current processing cluster
        inc(totalClusterPixels);

        // this accumulate red, green and blue values of all pixels in a cluster
        accumulatedRedValues := accumulatedRedValues +  currentPixelColorR;
        accumulatedGreenValues := accumulatedGreenValues + currentPixelColorG;
        accumulatedBlueValues := accumulatedBlueValues + currentPixelColorB;

        // updating the processed image pixels
        row2[b].rgbtRed   := Round(accumulatedRedValues / totalClusterPixels);
        row2[b].rgbtGreen := Round(accumulatedGreenValues / totalClusterPixels);
        row2[b].rgbtBlue  := Round(accumulatedBlueValues / totalClusterPixels);
        //rgb(GetRValue(TColor(prop.randomColorPointsArray[c])), GetGValue(TColor(prop.randomColorPointsArray[c])), GetBValue(TColor(prop.randomColorPointsArray[c])));

        end;
       end;
   end;

   // update input pixel color (updating the centroid)
   inputPointColor := rgb(Round(accumulatedRedValues / totalClusterPixels),
                            Round(accumulatedGreenValues / totalClusterPixels),
                            Round(accumulatedBlueValues / totalClusterPixels));

    end;

  end;

mImage:=temp2;
result:=mImage;
end;


function TIcal.msa3(const nInputPoint,iterasi,Radius: integer; const img: timage): TBitmap;
var
  temp2,temp1:tbitmap;
  inputPointColor  : TColor;
  inputPointRed, inputPointGreen, inputPointBlue : integer;
  currentPixelRedDistance, currentPixelBlueDistance, currentPixelGreenDistance : integer;
  currentPixelColorR,currentPixelColorG,currentPixelColorB:byte;
  i,j,a,b,c,d,e : integer;
  accumulatedRedValues,accumulatedGreenValues,accumulatedBlueValues : double;
  totalClusterPixels : integer;
  row1,row2:PRGBTripleArray;
begin
temp1:=img.Picture.Bitmap;
temp2:=tbitmap.create;
temp2.Width:=temp1.Width;
temp2.Height:=temp1.Height;
temp2.PixelFormat:=pf24bit;
mImage:=img.Picture.Bitmap;

for i:=0 to temp1.Height-1 do
  begin
  row1:=Mimage.ScanLine[i];
  row2:=temp2.ScanLine[i];
    for j:=0 to MImage.Width-1 do
      begin
      row2[j].rgbtRed:=row1[j].rgbtRed;
      row2[j].rgbtGreen:=row1[j].rgbtGreen;
      row2[j].rgbtBlue:=row1[j].rgbtBlue;
      end;
  end;



   // for making windows input points
//for e := 0 to nInputPoint-1 do
   for c := 0 to nInputPoint-1 do
   begin

    inputPointColor :=
    TColor(
    img.Canvas.Pixels[round(temp1.width/random(temp1.Width)),
                      round(temp1.height/random(temp1.Height))]);


    // given input point updated and processed prop.numberOfIterations times
    for d := 1 to iterasi do
    begin

    inputPointRed := GetRValue(inputPointColor);
    inputPointGreen := GetGValue(inputPointColor);
    inputPointBlue := GetBValue(inputPointColor);

    accumulatedRedValues := 0;
    accumulatedGreenValues := 0;
    accumulatedBlueValues := 0;
    totalClusterPixels := 0;

    // for travel through image pixels
    for a := 0 to temp1.height-1 do
    begin
    row1:=temp1.ScanLine[a];
    row2:=temp2.ScanLine[a];

      for b := 0 to temp1.width-1 do
      begin

        //---------------------------------------
        currentPixelColorR :=  row1[b].rgbtRed;
        currentPixelColorG :=  row1[b].rgbtGreen;
        currentPixelColorB :=  row1[b].rgbtBlue;

        currentPixelRedDistance := currentPixelColorR - inputPointRed;
        currentPixelGreenDistance := currentPixelColorG - inputPointGreen;
        currentPixelBlueDistance := currentPixelColorB - inputPointBlue;

        // calculate euclidian distance (it's square root to be taken, but square values mathches here)
        if (((currentPixelRedDistance * currentPixelRedDistance) +
             (currentPixelGreenDistance * currentPixelGreenDistance) +
             (currentPixelBlueDistance * currentPixelBlueDistance)) < Radius) then //1000 konstanta, bisa di-ubah2
        begin


        // totalClusterPixels hold how many pixels belong to a current processing cluster
        inc(totalClusterPixels);

        // this accumulate red, green and blue values of all pixels in a cluster
        accumulatedRedValues := accumulatedRedValues +  currentPixelColorR;
        accumulatedGreenValues := accumulatedGreenValues + currentPixelColorG;
        accumulatedBlueValues := accumulatedBlueValues + currentPixelColorB;

        // updating the processed image pixels
        row2[b].rgbtRed   := Round(accumulatedRedValues / totalClusterPixels);
        row2[b].rgbtGreen := Round(accumulatedGreenValues / totalClusterPixels);
        row2[b].rgbtBlue  := Round(accumulatedBlueValues / totalClusterPixels);
        //rgb(GetRValue(TColor(prop.randomColorPointsArray[c])), GetGValue(TColor(prop.randomColorPointsArray[c])), GetBValue(TColor(prop.randomColorPointsArray[c])));

        end;
       end;
   end;

   // update input pixel color (updating the centroid)
   inputPointColor := rgb(Round(accumulatedRedValues / totalClusterPixels),
                            Round(accumulatedGreenValues / totalClusterPixels),
                            Round(accumulatedBlueValues / totalClusterPixels));

    end;

  end;

mImage:=temp2;
result:=mImage;
end;


function TIcal.msaGrey(const iterasi,nInputPoint,Radius:integer;const img:Timage):TBitmap;
var
  temp2,temp1:TBitmap;
  inputPointColor  : TColor;
  inputPoint : integer;
  currentPixelDistance: integer;
  currentPixelColor:byte;
  i,j,a,b,c,d,e : integer;
  accumulatedValues : double;
  totalClusterPixels : integer;
  row1,row2:PRGBTripleArray;
  Clr:TColor; ClrGray:byte;

begin

temp1:=img.Picture.Bitmap;
Mimage:=img.Picture.Bitmap;

temp2:=tbitmap.create;
temp2.Width:=temp1.Width;
temp2.Height:=temp1.Height;
temp2.PixelFormat:=pf24bit;

  for i:=0 to temp1.Height-1 do
  begin
  row1:=Mimage.ScanLine[i];
  row2:=temp2.ScanLine[i];
    for j:=0 to MImage.Width-1 do
    begin
      Clr := rgb(row1[j].rgbtRed,row1[j].rgbtGreen,row1[j].rgbtBlue);
      ClrGray := RgbToGray (Clr);
      row2[j].rgbtBlue := ClrGray;
      row2[j].rgbtRed := ClrGray;
      row2[j].rgbtGreen := ClrGray;
    end;
  end;


//  mimage:=temp2;




   // for making windows for input points
for e := 0 to nInputPoint-1 do
   for c := 0 to nInputPoint-1 do
   begin

    inputPointColor := TColor(img.Canvas.Pixels[round(e*temp1.width/(nInputPoint+1)),round(c*temp1.height/(nInputPoint)+1)]);
    // given input point updated and processed prop.numberOfIterations times
    for d := 1 to iterasi do
    begin

    inputPoint:= GetRValue(inputPointColor);

    accumulatedValues := 0;
    totalClusterPixels := 0;

    // for travel through image pixels
    for a := 0 to temp1.height-1 do
    begin
    row1:=temp1.ScanLine[a];
    row2:=temp2.ScanLine[a];

      for b := 0 to temp1.width-1 do
      begin

        //---------------------------------------
        currentPixelColor :=  row1[b].rgbtRed;

        currentPixelDistance := currentPixelColor - inputPoint;

        // calculate euclidian distance (it's square root to be taken, but square values mathches here)
        if ((currentPixelDistance * currentPixelDistance) < Radius) then //1000 konstanta, bisa di-ubah2
        begin


        // totalClusterPixels hold how many pixels belong to a current processing cluster
        inc(totalClusterPixels);

        // this accumulate red, green and blue values of all pixels in a cluster
        accumulatedValues := accumulatedValues +  currentPixelColor;

        // updating the processed image pixels
        row2[b].rgbtRed   := Round(accumulatedValues / totalClusterPixels);
        row2[b].rgbtGreen := Round(accumulatedValues / totalClusterPixels);
        row2[b].rgbtBlue  := Round(accumulatedValues / totalClusterPixels);
        //rgb(GetRValue(TColor(prop.randomColorPointsArray[c])), GetGValue(TColor(prop.randomColorPointsArray[c])), GetBValue(TColor(prop.randomColorPointsArray[c])));

        end;
       end;
   end;

   // update input pixel color (updating the centroid)
   inputPointColor := rgb(Round(accumulatedValues / totalClusterPixels),
                            Round(accumulatedValues / totalClusterPixels),
                            Round(accumulatedValues / totalClusterPixels));

    end;

  end;

mimage:=temp2;
result:=mImage;
end;


function TIcal.ColorDetection(const cl1, cl2: TColor;
  tolerance: real): boolean;
var
r1,g1,b1,r2,g2,b2 : byte;
Tol : byte;
begin
SplitRGB(cl2,r2,g2,b2);
SplitRGB(cl1,r1,g1,b1);
Tol := round(Tolerance / 100 * 255);
   if (abs(r1-r2) <= Tol)and (abs(g1-g2) <= Tol)and (abs(b1-b2) <= Tol)then
     result:=true
  else result:=false;
end;

function TICal.SobelEdge(ABias: integer;const img:Timage) : TBitmap;
Var
  ABitmap:Tbitmap;
  LRow1, LRow2, LRow3, LRowOut : PRGBTripleArray;
  LRow, LCol : integer;
  LNewBlue                          : Extended;
  LNewGreen, LNewRed : Extended;
  LCoef : Extended;
begin

  mimage:=img.Picture.Bitmap;
  mask[0,0]:=-1;mask[1,0]:=-1;mask[2,0]:=-1;
  mask[0,1]:=-1;mask[1,1]:= 8;mask[2,1]:=-1;
  mask[0,2]:=-1;mask[1,2]:=-1;mask[2,2]:=-1;

  ABitmap:=mImage;
  LCoef := 0;
  for LRow := 0 to 2 do
    for LCol := 0 to 2 do
      LCoef := LCoef + Mask[LCol, LRow];

  if LCoef = 0 then LCoef := 1;

  mImage := TBitmap.Create;

  mImage.Width := ABitmap.Width - 2;
  mImage.Height := ABitmap.Height - 2;
  mImage.PixelFormat := pf24bit;

  LRow2 := ABitmap.ScanLine[0];
  LRow3 := ABitmap.ScanLine[1];

  for LRow := 1 to ABitmap.Height - 2 do
  begin

    LRow1 := LRow2;
    LRow2 := LRow3;
    LRow3 := ABitmap.ScanLine[LRow + 1];

    LRowOut := mImage.ScanLine[LRow-1];

    for LCol := 1 to ABitmap.Width - 2 do begin

      LNewBlue :=
        (LRow1[LCol-1].rgbtBlue*Mask[0,0]) + (LRow1[LCol].rgbtBlue*Mask[1,0]) + (LRow1[LCol+1].rgbtBlue*Mask[2,0]) +
        (LRow2[LCol-1].rgbtBlue*Mask[0,1]) + (LRow2[LCol].rgbtBlue*Mask[1,1]) + (LRow2[LCol+1].rgbtBlue*Mask[2,1]) +
        (LRow3[LCol-1].rgbtBlue*Mask[0,2]) + (LRow3[LCol].rgbtBlue*Mask[1,2]) + (LRow3[LCol+1].rgbtBlue*Mask[2,2]);
      LNewBlue := (LNewBlue / LCoef) + ABias;
      if LNewBlue > 255 then LNewBlue := 255;
      if LNewBlue < 0 then LNewBlue := 0;

      LNewGreen :=
        (LRow1[LCol-1].rgbtGreen*Mask[0,0]) + (LRow1[LCol].rgbtGreen*Mask[1,0]) + (LRow1[LCol+1].rgbtGreen*Mask[2,0]) +
        (LRow2[LCol-1].rgbtGreen*Mask[0,1]) + (LRow2[LCol].rgbtGreen*Mask[1,1]) + (LRow2[LCol+1].rgbtGreen*Mask[2,1]) +
        (LRow3[LCol-1].rgbtGreen*Mask[0,2]) + (LRow3[LCol].rgbtGreen*Mask[1,2]) + (LRow3[LCol+1].rgbtGreen*Mask[2,2]);
      LNewGreen := (LNewGreen / LCoef) + ABias;

      if LNewGreen > 255 then
        LNewGreen := 255;
      if LNewGreen < 0 then
        LNewGreen := 0;

      LNewRed :=
        (LRow1[LCol-1].rgbtRed*Mask[0,0]) + (LRow1[LCol].rgbtRed*Mask[1,0]) + (LRow1[LCol+1].rgbtRed*Mask[2,0]) +
        (LRow2[LCol-1].rgbtRed*Mask[0,1]) + (LRow2[LCol].rgbtRed*Mask[1,1]) + (LRow2[LCol+1].rgbtRed*Mask[2,1]) +
        (LRow3[LCol-1].rgbtRed*Mask[0,2]) + (LRow3[LCol].rgbtRed*Mask[1,2]) + (LRow3[LCol+1].rgbtRed*Mask[2,2]);
      LNewRed := (LNewRed / LCoef) + ABias;
      if LNewRed > 255 then
          LNewRed := 255;
      if LNewRed < 0 then
          LNewRed := 0;

      LRowOut[LCol-1].rgbtBlue  := trunc(LNewBlue);
      LRowOut[LCol-1].rgbtGreen := trunc(LNewGreen);
      LRowOut[LCol-1].rgbtRed   := trunc(LNewRed);

    end;

  end;
//binarisasi(mImage,contrass);

result:=mImage;

end;


function TICal.SobelEdge2(Sg1,Sg2:TstringGrid;ABias : integer;const img:Timage) : TBitmap;
Var
  ABitmap:Tbitmap;
  LRow1, LRow2, LRow3, LRowOut : PRGBTripleArray;
  LRow, LCol : integer;
  LNewBlue, LNewGreen, LNewRed : Extended;
  LNewBlue2, LNewGreen2, LNewRed2 : Extended;
  LCoef : Extended;
begin

  mimage:=img.Picture.Bitmap;
  MaskX[0,0]:=strtoint(sg1.cells[0,0]);
  MaskX[0,1]:=strtoint(sg1.cells[1,0]);
  MaskX[0,2]:=strtoint(sg1.cells[2,0]);
  MaskX[1,0]:=strtoint(sg1.cells[0,1]);
  MaskX[1,1]:=strtoint(sg1.cells[1,1]);
  MaskX[1,2]:=strtoint(sg1.cells[2,1]);
  MaskX[2,0]:=strtoint(sg1.cells[0,2]);
  MaskX[2,1]:=strtoint(sg1.cells[1,2]);
  MaskX[2,2]:=strtoint(sg1.cells[2,2]);

  MaskY[0,0]:=strtoint(Sg2.cells[0,0]);
  MaskY[0,1]:=strtoint(Sg2.cells[1,0]);
  MaskY[0,2]:=strtoint(Sg2.cells[2,0]);
  MaskY[1,0]:=strtoint(Sg2.cells[0,1]);
  MaskY[1,1]:=strtoint(Sg2.cells[1,1]);
  MaskY[1,2]:=strtoint(Sg2.cells[2,1]);
  MaskY[2,0]:=strtoint(Sg2.cells[0,2]);
  MaskY[2,1]:=strtoint(Sg2.cells[1,2]);
  MaskY[2,2]:=strtoint(Sg2.cells[2,2]);


  ABitmap:=mImage;
  LCoef := 0;
  for LRow := 0 to 2 do
    for LCol := 0 to 2 do
      LCoef := LCoef + Mask[LCol, LRow];

  if LCoef = 0 then LCoef := 1;

  mImage := TBitmap.Create;

  mImage.Width := ABitmap.Width - 2;
  mImage.Height := ABitmap.Height - 2;
  mImage.PixelFormat := pf24bit;

  LRow2 := ABitmap.ScanLine[0];
  LRow3 := ABitmap.ScanLine[1];

  for LRow := 1 to ABitmap.Height - 2 do
  begin

    LRow1 := LRow2;
    LRow2 := LRow3;
    LRow3 := ABitmap.ScanLine[LRow + 1];

    LRowOut := mImage.ScanLine[LRow-1];

    for LCol := 1 to ABitmap.Width - 2 do begin

      LNewBlue :=
        (LRow1[LCol-1].rgbtBlue*MaskX[0,0]) + (LRow1[LCol].rgbtBlue*MaskX[1,0]) + (LRow1[LCol+1].rgbtBlue*MaskX[2,0]) +
        (LRow2[LCol-1].rgbtBlue*MaskX[0,1]) + (LRow2[LCol].rgbtBlue*MaskX[1,1]) + (LRow2[LCol+1].rgbtBlue*MaskX[2,1]) +
        (LRow3[LCol-1].rgbtBlue*MaskX[0,2]) + (LRow3[LCol].rgbtBlue*MaskX[1,2]) + (LRow3[LCol+1].rgbtBlue*MaskX[2,2]);
      LNewBlue := (LNewBlue / LCoef) + ABias;

      if LNewBlue > 255 then LNewBlue := 255;
      if LNewBlue < 0 then LNewBlue := 0;

      LNewGreen :=
        (LRow1[LCol-1].rgbtGreen*MaskX[0,0]) + (LRow1[LCol].rgbtGreen*MaskX[1,0]) + (LRow1[LCol+1].rgbtGreen*MaskX[2,0]) +
        (LRow2[LCol-1].rgbtGreen*MaskX[0,1]) + (LRow2[LCol].rgbtGreen*MaskX[1,1]) + (LRow2[LCol+1].rgbtGreen*MaskX[2,1]) +
        (LRow3[LCol-1].rgbtGreen*MaskX[0,2]) + (LRow3[LCol].rgbtGreen*MaskX[1,2]) + (LRow3[LCol+1].rgbtGreen*MaskX[2,2]);
      LNewGreen := (LNewGreen / LCoef) + ABias;

      if LNewGreen > 255 then LNewGreen := 255;
      if LNewGreen < 0 then LNewGreen := 0;

      LNewRed :=
        (LRow1[LCol-1].rgbtRed*MaskX[0,0]) + (LRow1[LCol].rgbtRed*MaskX[1,0]) + (LRow1[LCol+1].rgbtRed*MaskX[2,0]) +
        (LRow2[LCol-1].rgbtRed*MaskX[0,1]) + (LRow2[LCol].rgbtRed*MaskX[1,1]) + (LRow2[LCol+1].rgbtRed*MaskX[2,1]) +
        (LRow3[LCol-1].rgbtRed*MaskX[0,2]) + (LRow3[LCol].rgbtRed*MaskX[1,2]) + (LRow3[LCol+1].rgbtRed*MaskX[2,2]);
      LNewRed := (LNewRed / LCoef) + ABias;

      if LNewRed > 255 then LNewRed := 255;
      if LNewRed < 0 then LNewRed := 0;


         ////tambahan dimari
         LNewBlue2 :=
        (LRow1[LCol-1].rgbtBlue*MaskY[0,0]) + (LRow1[LCol].rgbtBlue*MaskY[1,0]) + (LRow1[LCol+1].rgbtBlue*MaskY[2,0]) +
        (LRow2[LCol-1].rgbtBlue*MaskY[0,1]) + (LRow2[LCol].rgbtBlue*MaskY[1,1]) + (LRow2[LCol+1].rgbtBlue*MaskY[2,1]) +
        (LRow3[LCol-1].rgbtBlue*MaskY[0,2]) + (LRow3[LCol].rgbtBlue*MaskY[1,2]) + (LRow3[LCol+1].rgbtBlue*MaskY[2,2]);
      LNewBlue2 := (LNewBlue2 / LCoef) + ABias;
      if LNewBlue2 > 255 then LNewBlue2 := 255;
      if LNewBlue2 < 0 then LNewBlue2 := 0;

      LNewGreen2 :=
        (LRow1[LCol-1].rgbtGreen*MaskY[0,0]) + (LRow1[LCol].rgbtGreen*MaskY[1,0]) + (LRow1[LCol+1].rgbtGreen*MaskY[2,0]) +
        (LRow2[LCol-1].rgbtGreen*MaskY[0,1]) + (LRow2[LCol].rgbtGreen*MaskY[1,1]) + (LRow2[LCol+1].rgbtGreen*MaskY[2,1]) +
        (LRow3[LCol-1].rgbtGreen*MaskY[0,2]) + (LRow3[LCol].rgbtGreen*MaskY[1,2]) + (LRow3[LCol+1].rgbtGreen*MaskY[2,2]);
      LNewGreen2 := (LNewGreen2 / LCoef) + ABias;

      if LNewGreen2 > 255 then
        LNewGreen2 := 255;
      if LNewGreen2 < 0 then
        LNewGreen2 := 0;

      LNewRed2 :=
        (LRow1[LCol-1].rgbtRed*MaskY[0,0]) + (LRow1[LCol].rgbtRed*MaskY[1,0]) + (LRow1[LCol+1].rgbtRed*MaskY[2,0]) +
        (LRow2[LCol-1].rgbtRed*MaskY[0,1]) + (LRow2[LCol].rgbtRed*MaskY[1,1]) + (LRow2[LCol+1].rgbtRed*MaskY[2,1]) +
        (LRow3[LCol-1].rgbtRed*MaskY[0,2]) + (LRow3[LCol].rgbtRed*MaskY[1,2]) + (LRow3[LCol+1].rgbtRed*MaskY[2,2]);
      LNewRed2 := (LNewRed2 / LCoef) + ABias;
      if LNewRed2 > 255 then
          LNewRed2 := 255;
      if LNewRed2 < 0 then
          LNewRed2:= 0;
         ///////tambah disini
      LRowOut[LCol-1].rgbtBlue  := trunc(sqrt(LNewBlue*LNewBlue+LNewBlue2*LNewBlue2));
      LRowOut[LCol-1].rgbtGreen := trunc(sqrt(LNewGreen*LNewGreen+LNewGreen2+LNewGreen2));
      LRowOut[LCol-1].rgbtRed   := trunc(sqrt(LNewRed*LNewRed+LNewRed2*LNewRed2));

    end;

  end;

//binarisasi(mimage, contrass);
result:=mImage;

end;

procedure splitRGB(const cl1:TColor; var r,g,b : byte);
 begin
 r:=getRvalue(cl1);
 g:=getGvalue(cl1);
 b:=getBvalue(cl1);
 end;

procedure TIcal.countRecursive(var image13: TImage; const curcolor: TColor;
  const X, Y, Tolerance: integer; var area: tkordinatimage);
begin

if (X+1 >=image13.Picture.Bitmap.Width)  or
      (Y+1 >= image13.Picture.Bitmap.Height)or
      (X-1<0)or
      (Y-1<0)
      then  exit;


  //membagi menjadi sub image berdasarkan kesamaan warna , di cek ke 8 arah
      image13.Canvas.Pixels[X,Y]:=clFuchsia;


      inc(area.luas);
      area.posisi[area.luas].x:=x;
      area.posisi[area.luas].y:=y;

   if ColorDetection(image13.Canvas.Pixels[X+1,Y],curColor,Tolerance) then //cek ke kanan
       countRecursive(image13,curColor,X+1,Y,Tolerance,area);
   if ColorDetection(image13.Canvas.Pixels[X-1,Y],curColor,Tolerance) then //cek ke kiri
       countRecursive(image13,curColor,X-1,Y,Tolerance,area);
   if ColorDetection(image13.Canvas.Pixels[X,Y-1],curColor,Tolerance) then //cek ke atas
       countRecursive(image13,curColor,X,Y-1,Tolerance,area);
   if ColorDetection(image13.Canvas.Pixels[X,Y+1],curColor,Tolerance) then //cek ke bawah
       countRecursive(image13,curColor,X,Y+1,Tolerance,area);
   if ColorDetection(image13.Canvas.Pixels[X+1,Y+1],curColor,Tolerance) then //cek ke kanan bawah
       countRecursive(image13,curColor,X+1,Y+1,Tolerance,area);
   if ColorDetection(image13.Canvas.Pixels[X-1,Y+1],curColor,Tolerance) then //cek ke kiri bawah
       countRecursive(image13,curColor,X-1,Y+1,Tolerance,area);
   if ColorDetection(image13.Canvas.Pixels[X-1,Y-1],curColor,Tolerance) then //cek ke kiri atas
       countRecursive(image13,curColor,X-1,Y-1,Tolerance,area);
   if ColorDetection(image13.Canvas.Pixels[X+1,Y-1],curColor,Tolerance)then //cek ke kanan atas
       countRecursive(image13,curColor,X+1,Y-1,Tolerance,area);


end;

function nilaivector(titik1, titik2: tpoint): double;
begin
if sqrt(sqr(titik1.x-titik2.x)+sqr(titik1.y-titik2.y))>0 then

     result:= sqrt(sqr(titik1.x-titik2.x)+sqr(titik1.y-titik2.y))
else
 Result:=0.001;
end;

function TIcal.lokalisasi(ToleransiLokalisasi,minpixelsvalue:Integer;const img:Timage): TBitmap;
var i,j:integer;
temp:timage;
row1:PRGBTripleArray;
begin
 jumArrayKI:=0;
 temp:=Timage.Create(nil);
 temp.Picture.Bitmap:=img.Picture.Bitmap;

  for i:=0 to temp.Picture.Width-1 do
    for j:=0 to temp.Picture.Height-1 do
    begin
          if (temp.Canvas.Pixels[i,j]=CLWhite) then
          begin
              arrayKordinatImage[jumArrayKI].luas:=0;
              countRecursive(temp,temp.Canvas.Pixels[i,j],i,j,ToleransiLokalisasi,arrayKordinatImage[jumArrayKI]);
              if  arrayKordinatImage[jumArrayKI].luas>=minpixelsvalue then
                  inc(jumArrayKI);
          end;
    end;


 for i:=0 to mImage.Height-1 do
 begin
 row1:=temp.Picture.Bitmap.ScanLine[i];
  for j:=0 to mImage.Width-1 do
     begin
        row1[j].rgbtRed:=255;
        row1[j].rgbtGreen:=255;
        row1[j].rgbtBlue:=255;
     end;
 end;


 for i:=0 to jumArrayKI-1 do
  for j:=0 to  arrayKordinatImage[i].luas do
   temp.Canvas.Pixels[arrayKordinatImage[i].posisi[j].x,arrayKordinatImage[i].posisi[j].y]:= clRed;
//   clcawal.Canvas.Pixels[arrayKordinatImage[i].posisi[j].x,arrayKordinatImage[i].posisi[j].y];
mImage:=temp.Picture.Bitmap;
result:=mImage;

end;



function TIcal.binarisasi(casal:TBitmap;top: integer):tbitmap;
var
r1,g1,b1: byte;
temp:tbitmap;
i,j ,bot:integer;
row1,row2:PRGBTripleArray;
begin
 bot:=top-1;
 r1:=0;g1:=0;b1:=0;
 temp:=tbitmap.Create;
 temp.Width:=casal.Width;
 temp.Height:=casal.Height;
 temp.PixelFormat:=pf24bit;

 for i:=0 to casal.Height-1 do
 begin
 row1:=casal.ScanLine[i];
 row2:=temp.ScanLine[i];//mImage.ScanLine[i];
     for j:=0 to casal.Width-1 do
      begin

       if row1[j].rgbtRed < bot then r1:=0
        else if row1[j].rgbtRed >= top then r1:= 255  ;

       if row1[j].rgbtGreen < bot then g1:=0
        else if row1[j].rgbtGreen >= top then g1:= 255 ;

       if row1[j].rgbtBlue < bot then b1:=0
        else if row1[j].rgbtBlue >= top then b1:= 255  ;

      // if row1[j].rgbtRed+row1[j].rgbtGreen+row1[j].rgbtBlue>top then
       if r1+g1+b1 > 0 then
          begin
            row2[j].rgbtRed:=255;
            row2[j].rgbtGreen:=255;
            row2[j].rgbtBlue:=255;
          end
       else
          begin
            row2[j].rgbtRed:=0;
            row2[j].rgbtGreen:=0;
            row2[j].rgbtBlue:=0;
          end;


      end;

 end;
mimage:=temp;
result:=mimage;
end;

destructor TIcal.free;
begin
mimage.Free;

end;

function TIcal.analisaBentuk(sourceimg: timage; warna: tcolor;
  amer: double;index:integer): tbitmap;
var
 a,b,c,d:tpoint;
i,j:integer;
tested:tkordinatimage;
temp:Timage;
//pixelValue:integer;

begin
tested:=arrayKordinatImage[index];


a.x:=10000;
a.Y:=10000;

b.X:=0;
b.Y:=10000;

c.Y:=0;
c.X:=0;

d.X:=10000;
d.Y:=0;

      for j:=1 to tested.luas-1 do
      begin
         if tested.posisi[j].X<a.X then  //xmin
            begin
              a.X:=tested.posisi[j].X;
              d.X:=tested.posisi[j].X;
            end;
         if tested.posisi[j].y<a.Y then //ymin
            begin
              a.y:=tested.posisi[j].y;
              b.y:=tested.posisi[j].y;
            end;
         if tested.posisi[j].X>b.X then //xmax
            begin
              b.X:=tested.posisi[j].X;
              c.X:=tested.posisi[j].X;
            end;
         if tested.posisi[j].y>d.y then //ymax
            begin
              d.y:=tested.posisi[j].y;
              c.y:=tested.posisi[j].y;
            end;
      end;
     {
      A(xmin,ymin)                  B(xmax,ymin)
      *-----------------------------*
      |                             |
      |                             |
      *-----------------------------*
      D(xmin,ymax)                  C(xmax,ymax)
      }
   //  gambarpersegi(image13,a,b,c,d);

  // pixelValue:=0;

temp:=timage.Create(nil);
temp.Picture.Bitmap:=tbitmap.Create;
temp.Picture.Bitmap.Width:=round(nilaivector(a,b)+1);
temp.Picture.Bitmap.Height:=round(nilaivector(a,d)+1);
temp.Picture.Bitmap.PixelFormat:=pf24bit;

   for i:=a.X to b.X do
     for j:=a.Y to d.Y do
      begin
       temp.Canvas.Pixels[i-a.X,j-a.Y]:=sourceimg.Canvas.Pixels[i,j];
       //pixelvalue:=pixelValue + GetRValue(sourceimg.Canvas.Pixels[i,j]);
      end;
   tested.R:=nilaivector(a,b)/nilaivector(a,d);
   tested.Ao:=(nilaivector(a,b)*nilaivector(a,d));
   tested.Ed:=2*nilaivector(a,b)+2*nilaivector(a,d);

arrayKordinatImage[index]:=tested;

result:=temp.picture.bitmap;

end;

Procedure TIcal.PanalisaBentuk(sourceimg: timage; warna: tcolor;
  amer: double;index:integer);
var
 a,b,c,d:tpoint;
j:integer;
tested:tkordinatimage;
//pixelValue:integer;

begin
tested:=arrayKordinatImage[index];


a.x:=10000;
a.Y:=10000;

b.X:=0;
b.Y:=10000;

c.Y:=0;
c.X:=0;

d.X:=10000;
d.Y:=0;

      for j:=1 to tested.luas-1 do
      begin
         if tested.posisi[j].X<a.X then  //xmin
            begin
              a.X:=tested.posisi[j].X;
              d.X:=tested.posisi[j].X;
            end;
         if tested.posisi[j].y<a.Y then //ymin
            begin
              a.y:=tested.posisi[j].y;
              b.y:=tested.posisi[j].y;
            end;
         if tested.posisi[j].X>b.X then //xmax
            begin
              b.X:=tested.posisi[j].X;
              c.X:=tested.posisi[j].X;
            end;
         if tested.posisi[j].y>d.y then //ymax
            begin
              d.y:=tested.posisi[j].y;
              c.y:=tested.posisi[j].y;
            end;
      end;
      {
      A(xmin,ymin)                  B(xmax,ymin)
      *-----------------------------*
      |                             |
      |                             |
      *-----------------------------*
      D(xmin,ymax)                  C(xmax,ymax)
      }
   //  gambarpersegi(image13,a,b,c,d);

  { pixelValue:=0;

   for i:=a.X to b.X do
     for j:=a.Y to d.Y do
      begin
      //temp.Canvas.Pixels[i-a.X,j-a.Y]:= sourceimg.Canvas.Pixels[i,j];
       pixelvalue:=pixelValue + GetRValue(sourceimg.Canvas.Pixels[i,j]);
      end; }
   tested.R:=nilaivector(a,b)/nilaivector(a,d);
   tested.Ao:=nilaivector(a,b)*nilaivector(a,d);
   tested.Ed:=2*nilaivector(a,b)+2*nilaivector(a,d);

arrayKordinatImage[index]:=tested;

end;

function TIcal.MClassification(x_uji:Tvector; statistik,
  covariance: T3x3FloatArray): boolean;
  var i:integer;
  c1,c2,c3,d1,d2,d3:extended;

begin
   c1:=sqrt(sqr(covariance[0,0])+sqr(covariance[1,0])+sqr(covariance[2,0]));
   c2:=sqrt(sqr(covariance[0,1])+sqr(covariance[1,1])+sqr(covariance[2,1]));
   c3:=sqrt(sqr(covariance[0,2])+sqr(covariance[1,2])+sqr(covariance[2,2]));
   d1:=0;d2:=0;d3:=0;
   for i:=0 to 2 do
   begin
     d1:=d1+(x_uji[i]-statistik[i,0])*(covariance[i,0]/c1);
     d2:=d2+(x_uji[i]-statistik[i,1])*(covariance[i,1]/c2);
     d3:=d3+(x_uji[i]-statistik[i,2])*(covariance[i,2]/c3);

   end;

   d1:= sqrt(sqr(d1*x_uji[0])+sqr(d1*x_uji[1])+sqr(d1*x_uji[2]));
   d2:= sqrt(sqr(d2*x_uji[0])+sqr(d2*x_uji[1])+sqr(d2*x_uji[2]));
   d3:= sqrt(sqr(d3*x_uji[0])+sqr(d3*x_uji[1])+sqr(d3*x_uji[2]));
 //showmessage(floattostr(d1)+'|'+floattostr(d3));
 if (d1>d3)or(d2>d3) then
   result:=true
   else Result := false;
end;

function invert(Casal: Tbitmap): tbitmap;
var
i,j:integer;
row2,row1:PRGBTripleArray;

begin
result:=Tbitmap.Create;
result.Width:=casal.Width;
result.Height:=Casal.Height;
result.PixelFormat:=pf24bit;

 for i:=0 to casal.Height-1 do
  begin
  row1:=casal.ScanLine[i];
  row2:=result.ScanLine[i];
    for j:=0 to casal.Width-1 do
      begin
        row2[j].rgbtBlue:=255-row1[i].rgbtBlue;
        row2[j].rgbtRED:=255-row1[i].rgbtRED;
        row2[j].rgbtGReen:=255-row1[i].rgbtGreen;
      end;
  end;
end;

function TIcal.drawChar(sourceimg: timage; warna: tcolor;
  index: integer): tBitmap;
var
 a,b,c,d:tpoint;
i,j:integer;
tested:tkordinatimage;
temp:Timage;


begin
tested:=arrayKordinatImage[index];


a.x:=10000;
a.Y:=10000;

b.X:=0;
b.Y:=10000;

c.Y:=0;
c.X:=0;

d.X:=10000;
d.Y:=0;

      for j:=1 to tested.luas-1 do
      begin
         if tested.posisi[j].X<a.X then  //xmin
            begin
              a.X:=tested.posisi[j].X;
              d.X:=tested.posisi[j].X;
            end;
         if tested.posisi[j].y<a.Y then //ymin
            begin
              a.y:=tested.posisi[j].y;
              b.y:=tested.posisi[j].y;
            end;
         if tested.posisi[j].X>b.X then //xmax
            begin
              b.X:=tested.posisi[j].X;
              c.X:=tested.posisi[j].X;
            end;
         if tested.posisi[j].y>d.y then //ymax
            begin
              d.y:=tested.posisi[j].y;
              c.y:=tested.posisi[j].y;
            end;
      end;
     {
      A(xmin,ymin)                  B(xmax,ymin)
      *-----------------------------*
      |                             |
      |                             |
      *-----------------------------*
      D(xmin,ymax)                  C(xmax,ymax)
      }
   //  gambarpersegi(image13,a,b,c,d);


temp:=timage.Create(nil);
temp.Picture.Bitmap:=tbitmap.Create;
temp.Picture.Bitmap.Width:=round(nilaivector(a,b)+1);
temp.Picture.Bitmap.Height:=round(nilaivector(a,d)+1);
temp.Picture.Bitmap.PixelFormat:=pf24bit;

   for i:=0 to tested.luas do
      temp.Canvas.Pixels[tested.posisi[i].X,tested.posisi[i].y]:=warna;

   tested.R:=nilaivector(a,b)/nilaivector(a,d);
   tested.Ao:=(nilaivector(a,b)*nilaivector(a,d));
   tested.Ed:=2*nilaivector(a,b)+2*nilaivector(a,d);

arrayKordinatImage[index]:=tested;

result:=temp.picture.bitmap;

end;

end.
