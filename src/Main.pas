unit Main;

interface

uses
  Windows, IcalClass, Messages, SysUtils, Variants, Classes,
  Graphics, Controls, Forms, Inifiles,
  Dialogs, StdCtrls, jpeg, ExtCtrls, Menus, Spin,
  ComCtrls,  DB, ADODB, DBCtrls, Grids, DBGrids, XPMan, FileCtrl;



type
  TForm1 = class(TForm)
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    exit1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CSegment: TImage;
    Panel1: TPanel;
    BtSegmen: TButton;
    BtLokalisasi: TButton;
    BtAnalisa: TButton;
    Label1: TLabel;
    Label2: TLabel;
    seInput: TSpinEdit;
    SeAmer: TSpinEdit;
    TabSheet4: TTabSheet;
    Ctepi: TImage;
    TabSheet6: TTabSheet;
    Clokal: TImage;
    btEdgeDet: TButton;
    SeMinPixels: TSpinEdit;
    Label3: TLabel;
    btReset: TButton;
    TabSheet7: TTabSheet;
    Chasil: TImage;
    SeKontrass: TSpinEdit;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    PopupMenu1: TPopupMenu;
    SimpanGambar1: TMenuItem;
    ListBox1: TListBox;
    PopupMenu2: TPopupMenu;
    Jadikandatatraining1: TMenuItem;
    TabSheet3: TTabSheet;
    TabSheet5: TTabSheet;
    TabSheet8: TTabSheet;
    Panel2: TPanel;
    Panel3: TPanel;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    DBNavigator1: TDBNavigator;
    ADOConnection1: TADOConnection;
    TTraining: TADOTable;
    QInsert: TADOQuery;
    CTraining: TImage;
    TTrainingNamaCitra: TWideStringField;
    TTrainingInputPoint: TIntegerField;
    TTrainingAmbangKontras: TIntegerField;
    TTrainingLuasMinimal: TIntegerField;
    TTrainingMER: TIntegerField;
    TTrainingAo: TBCDField;
    TTrainingRatio: TBCDField;
    TTrainingEdgeDensity: TBCDField;
    Cplat: TImage;
    TTrainingnomor: TIntegerField;
    Panel4: TPanel;
    DriveComboBox1: TDriveComboBox;
    DirectoryListBox1: TDirectoryListBox;
    FileListBox1: TFileListBox;
    SERadius: TSpinEdit;
    TTrainingtanggalinput: TDateTimeField;
    LBChar: TListBox;
    Panel5: TPanel;
    CChar0: TImage;
    insertasnonplate1: TMenuItem;
    QNonPlate: TADOQuery;
    TKlasifikasi: TADOTable;
    DataSource2: TDataSource;
    DBGrid2: TDBGrid;
    TKlasifikasiNamaKelas: TWideStringField;
    TKlasifikasiAo: TFloatField;
    TKlasifikasiC_Ao: TFloatField;
    TKlasifikasiR: TFloatField;
    TKlasifikasiC_R: TFloatField;
    TKlasifikasiEd: TFloatField;
    TKlasifikasiCEd: TFloatField;
    CChar1: TImage;
    CChar2: TImage;
    CChar3: TImage;
    CChar4: TImage;
    CChar5: TImage;
    CChar6: TImage;
    CChar7: TImage;
    Panel6: TPanel;
    CFoundedPlat: TImage;
    Image1: TImage;
    Image2: TImage;
    XPManifest1: TXPManifest;
    Sekontrass2: TSpinEdit;
    Label8: TLabel;
    seMin2: TSpinEdit;
    Label6: TLabel;
    btChar: TButton;
    TabSheet9: TTabSheet;
    Cbiner: TImage;
    QsimpanHasil: TADOQuery;
    TabSheet10: TTabSheet;
    TDataUji: TADOTable;
    IntegerField1: TIntegerField;
    WideStringField1: TWideStringField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    BCDField1: TBCDField;
    BCDField2: TBCDField;
    BCDField3: TBCDField;
    DateTimeField1: TDateTimeField;
    Panel7: TPanel;
    Image3: TImage;
    Panel8: TPanel;
    DBGrid3: TDBGrid;
    DBNavigator2: TDBNavigator;
    DataSource3: TDataSource;
    TDataUjibenar: TBooleanField;
    PopupMenu3: TPopupMenu;
    Refresh1: TMenuItem;
    PopupMenu4: TPopupMenu;
    Refresh2: TMenuItem;
    Panel9: TPanel;
    Cawal: TImage;
    Panel10: TPanel;
    SGopX: TStringGrid;
    Label4: TLabel;
    Label5: TLabel;
    sgopy: TStringGrid;
    StatusBar1: TStatusBar;
    Button1: TButton;
    OpenDialog1: TOpenDialog;
    SavePictureDialog1: TSaveDialog;
    procedure exit1Click(Sender: TObject);
    procedure BtLokalisasiClick(Sender: TObject);
    procedure BtAnalisaClick(Sender: TObject);
    procedure btEdgeDetClick(Sender: TObject);
    procedure btResetClick(Sender: TObject);
    procedure SimpanGambar1Click(Sender: TObject);
    procedure BtSegmenClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure Jadikandatatraining1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ListBox1KeyPress(Sender: TObject; var Key: Char);
    procedure DBNavigator1Click(Sender: TObject; Button: TNavigateBtn);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FileListBox1Change(Sender: TObject);
    procedure TabSheet1Enter(Sender: TObject);
    procedure Sekontrass2Change(Sender: TObject);
    procedure btCharClick(Sender: TObject);
    procedure LBCharClick(Sender: TObject);
    procedure insertasnonplate1Click(Sender: TObject);
    procedure SeKontrassChange(Sender: TObject);
    procedure TTrainingCalcFields(DataSet: TDataSet);
    procedure DBGrid3DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure DBGrid3TitleClick(Column: TColumn);
    procedure Refresh1Click(Sender: TObject);
    procedure Refresh2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure TDataUjiAfterDelete(DataSet: TDataSet);
    procedure DataSource3DataChange(Sender: TObject; Field: TField);
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
    procedure DataSource3UpdateData(Sender: TObject);
    procedure TDataUjiBeforeDelete(DataSet: TDataSet);
  private
    { Private declarations }

  public
    procedure gambarpersegi (var gambar:timage;a,b,c,d:tpoint);
    procedure loadDbFiles;
    { Public declarations }
  end;



function GetTempDirectory: String;


var
  Form1: TForm1;
  IcalImage:TIcal;
  PathExe,NamaFileBmp,namaDB,deletecitra,oldnamacitra,namacitra:string;
  nomor1,nomor2:integer;

  function resizeBMP(gbr:tbitmap;w:integer):tbitmap;

implementation

{$R *.dfm}

procedure TForm1.exit1Click(Sender: TObject);


begin
close;
end;


procedure TForm1.BtLokalisasiClick(Sender: TObject);
var i:integer;
begin
Clokal.Picture.Bitmap:=
    IcalImage.lokalisasi(0,SeMinPixels.Value,Cbiner);
PageControl1.TabIndex:=4;

label14.Caption :=(inttostr(IcalImage.jumArrayKI));
listbox1.Items.Clear;
for  i:=0 to IcalImage.jumArrayKI-1 do
  begin
     icalImage.PanalisaBentuk(cawal,clblue,SeAmer.value,i);
     ListBox1.Items.Add('C'+intTostr(i+1)
     +' |R: ' + format('%5.2f',[icalImage.arrayKordinatImage[i].R])
     +' |Ao: '+ format('%6.2f',[icalImage.arrayKordinatImage[i].Ao])
     +' |Ed: '+ format('%8.2f',[icalImage.arrayKordinatImage[i].Ed])
     );
  end;
end;

procedure TForm1.BtAnalisaClick(Sender: TObject);
var
jpeg:TJPEGImage;
uji:TVector;
ketemu,simpan:Boolean;
stat,cov:T3x3FloatArray;
i:integer;
begin
ketemu:=false;  simpan:=false;
     PageControl1.TabIndex:=5;

      I:=0;  TKLasifikasi.Open;
      TKlasifikasi.First;
      while (i<3) do
      begin
        stat[i,0]:=TKlasifikasi['R'];
        stat[i,1]:=TKlasifikasi['Ao'];
        stat[i,2]:=TKlasifikasi['Ed'];

        cov[i,0]:=TKlasifikasi['C_R'];
        cov[i,1]:=TKlasifikasi['C_Ao'];
        cov[i,2]:=TKlasifikasi['CEd'];

        TKlasifikasi.Next;
        inc(i);
      end;

      //PageControl1.TabIndex:=4;
      for i:=ListBox1.Items.Count-1 downto 0 do
      begin


      uji[0]:=icalimage.arrayKordinatImage[i].R;
      uji[1]:=icalimage.arrayKordinatImage[i].Ao;
      uji[2]:=icalimage.arrayKordinatImage[i].Ed;

      if (uji[1]>=1400) and
         (uji[1]<=3500) and
         (uji[0]<=3.5)    and
         (uji[0]>=2.2)  then

      begin
        if IcalImage.MClassification(uji,stat,cov) then
          begin

                  IcalImage.R:=uji[0];
                  IcalImage.Ao:=uji[1];
                  IcalImage.Ed:=uji[2];
                  chasil.Picture.Bitmap:=icalImage.analisaBentuk(cawal,clblue,Seamer.Value,i);
                  cFoundedPlat.Picture.Bitmap:=IcalImage.binarisasi(Chasil.Picture.Bitmap, SeKontrass2.Value);
                  if (MessageDlg('Plat nomor ditemukan pada kandidat '+inttostr(i+1)+', lanjut ke halaman berikut?',
                     mtInformation,[mbYes,mbNo],0)=mrYes) then
                  begin
                    PageControl1.TabIndex:=6;
                    ketemu:=true;
                    break;
                  end;


          end;
        end;
      end;
    if not ketemu then
    begin

      if (MessageDlg('Sistem tidak dapat mendeteksi plat nomor '+#13+' Simpan Percobaan?', mtConfirmation,[mbYes,mbNo],0)=mrYes)then
         simpan:=true;

   if simpan then
    begin
      QsimpanHasil.Close;
      QSimpanHasil.Parameters.ParamByName('benar').Value:=ketemu;
      QSimpanHasil.Parameters.ParamByName('namaCitra').Value:=NamaFileBmp;
      QSimpanHasil.Parameters.ParamByName('inputpoint').Value:=seInput.Value;
      QSimpanHasil.Parameters.ParamByName('ambangkontras').Value:=SeKontrass.Value;
      QSimpanHasil.Parameters.ParamByName('luasminimal').Value:=SeMinPixels.Value;
      QSimpanHasil.Parameters.ParamByName('mer').Value:=SeAmer.Value;
      QSimpanHasil.Parameters.ParamByName('ao').Value:=IcalImage.Ao;
      QSimpanHasil.Parameters.ParamByName('ratio').Value:=IcalImage.R;
      QSimpanHasil.Parameters.ParamByName('edgedensity').Value:=IcalImage.Ed;
      QSimpanHasil.Parameters.ParamByName('tanggalinput').Value:=now;
      QsimpanHasil.ExecSQL;
      TDataUji.close;
    TDataUji.open;
    jpeg:=TJPEGImage.Create;
    jpeg.Assign(cawal.Picture.Bitmap);
    jpeg.SaveToFile(ExtractFilePath(application.ExeName)+'\uji\'+NamaFileBmp);
    //jpeg.Free;
    end;


    end;
end;

procedure Tform1.gambarpersegi (var gambar:timage;a,b,c,d:tpoint);
var kotak:array[1..4]of tpoint;
begin
kotak[1]:=a;
kotak[2]:=b;
kotak[3]:=c;
kotak[4]:=d;

  with gambar.Canvas do begin
    Brush.Style := bsSolid;
    Brush.Color := clBlue;

    MoveTo(a.x,a.y);
    Polygon(kotak);
  end;

end;

procedure TForm1.BtSegmenClick(Sender: TObject);
begin
  CSegment.Picture.Bitmap:=IcalImage.msa(5,seInput.Value,SERadius.Value,Cawal);
  PageControl1.TabIndex:=1;
end;

procedure TForm1.btEdgeDetClick(Sender: TObject);
begin
ctepi.Picture.Bitmap :=
    IcalImage.SobelEdge2(sgOpx,sgopy,5,CSegment);

PageControl1.TabIndex:=3;
if (SeKontrass.Value=null) then     exit;
      Cbiner.Picture.Bitmap:=icalimage.binarisasi(Ctepi.Picture.Bitmap,SeKontrass.Value);

end;

procedure TForm1.btResetClick(Sender: TObject);
begin
csegment.Picture.Bitmap:=cawal.Picture.Bitmap;
chasil.Picture.Bitmap:=cawal.Picture.Bitmap;
ctepi.Picture.Bitmap:=cawal.Picture.Bitmap;
clokal.Picture.Bitmap:=cawal.Picture.Bitmap;
PageControl1.TabIndex:=0;
end;

procedure TForm1.SimpanGambar1Click(Sender: TObject);
var
bmp:Tbitmap;
jpeg:TjpegImage;
begin
if not panel1.Visible then exit;
if SavePictureDialog1.Execute then
begin
  bmp:=TBitmap.Create;
  jpeg:=TJPEGImage.Create;

    if PageControl1.TabIndex=0 then
      bmp:=cawal.Picture.Bitmap else
    if PageControl1.TabIndex=1 then
      bmp:=csegment.Picture.Bitmap else
    if PageControl1.TabIndex=2 then
      bmp:=ctepi.Picture.Bitmap else
    if PageControl1.TabIndex=3 then
      bmp:=cbiner.Picture.Bitmap else
    if PageControl1.TabIndex=4 then
      bmp:=clokal.Picture.Bitmap else
    if PageControl1.TabIndex=5 then
      bmp:=chasil.Picture.Bitmap  else
    if PageControl1.TabIndex=6 then
      bmp:=CFoundedPlat.Picture.Bitmap;

  if uppercase(ExtractFileExt(SavePictureDialog1.FileName))='.BMP' then
    bmp.SaveToFile(SavePictureDialog1.FileName)
  else
    if uppercase(ExtractFileExt(SavePictureDialog1.FileName))='.JPG' then
  begin
    //ShowMessage('');
    jpeg.Assign(bmp);
    jpeg.SaveToFile(SavePictureDialog1.FileName);

  end;

end;

end;

procedure TForm1.FormCreate(Sender: TObject);
var FileIni:TInifile;
begin
nomor1:=0;
nomor2:=0;

IcalImage:=TIcal.Create;
fileIni:=TIniFile.Create(GetTempDirectory+'\ical.INI');
 try
    seInput.Value:=FileIni.ReadInteger('Params','nInput',5);
    SeKontrass.Value:=FileIni.ReadInteger('Params','Kontrass',65);
    SeMinPixels.Value:=FileIni.ReadInteger('Params','MinPixels',175);
    SeAmer.Value:=FileIni.ReadInteger('Params','Amer',1000);
    seMin2.Value:=FileIni.ReadInteger('Params','SeMin2',25);
    Sekontrass2.Value:=FileIni.ReadInteger('Params','Kontrass2',150);

    SGopX.Cells[0,0]:=FileIni.ReadString('OperatorX','x00','0');
    SGopX.Cells[0,1]:=FileIni.ReadString('OperatorX','x01','0');
    SGopX.Cells[0,2]:=FileIni.ReadString('OperatorX','x02','0');
    SGopX.Cells[1,0]:=FileIni.ReadString('OperatorX','x10','0');
    SGopX.Cells[1,1]:=FileIni.ReadString('OperatorX','x11','0');
    SGopX.Cells[1,2]:=FileIni.ReadString('OperatorX','x12','0');
    SGopX.Cells[2,0]:=FileIni.ReadString('OperatorX','x20','0');
    SGopX.Cells[2,1]:=FileIni.ReadString('OperatorX','x21','0');
    SGopX.Cells[2,2]:=FileIni.ReadString('OperatorX','x22','0');

    SGopy.Cells[0,0]:=FileIni.ReadString('Operatory','y00','0');
    SGopy.Cells[0,1]:=FileIni.ReadString('Operatory','y01','0');
    SGopy.Cells[0,2]:=FileIni.ReadString('Operatory','y02','0');
    SGopy.Cells[1,0]:=FileIni.ReadString('Operatory','y10','0');
    SGopy.Cells[1,1]:=FileIni.ReadString('Operatory','y11','0');
    SGopy.Cells[1,2]:=FileIni.ReadString('Operatory','y12','0');
    SGopy.Cells[2,0]:=FileIni.ReadString('Operatory','y20','0');
    SGopy.Cells[2,1]:=FileIni.ReadString('Operatory','y21','0');
    SGopy.Cells[2,2]:=FileIni.ReadString('Operatory','y22','0');

    namaDb:=FileIni.ReadString('Database','NamaFile','db.mdb');
  finally
    fileIni.Free;
  end;
PathExe:=ExtractFilePath(Application.ExeName);


ADOConnection1.Close;
ADOConnection1.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+ExtractFilePath(application.ExeName)+'\'+namaDB+';Persist Security Info=False';
ADOConnection1.Connected:=true;
TTraining.Active:=true;
TDataUji.Active:=true;
loadDbFiles;
PageControl1.TabIndex:=0;
end;

procedure TForm1.PageControl1Change(Sender: TObject);
begin
//chasil.Picture.Bitmap:=IcalImage.mImage;

end;

procedure TForm1.ListBox1Click(Sender: TObject);

begin
     chasil.Picture.Bitmap:=icalImage.analisaBentuk(cawal,clblue,Seamer.Value,listbox1.ItemIndex);
     cFoundedPlat.Picture.Bitmap:=IcalImage.binarisasi(Chasil.Picture.Bitmap, SeKontrass2.Value);
end;

procedure TForm1.Jadikandatatraining1Click(Sender: TObject);
var
jpeg:TJPEGImage;
begin
if ListBox1.ItemIndex=-1 then exit;
    TTraining.First;
if  TTraining.Locate('namaCitra',NamaFileBmp,[loCaseInsensitive]) then
  begin
  showmessage('data sudah ada');
  exit;
  end;

QInsert.Close;
//(,,,,,,,)
qInsert.Parameters.ParamByName('namaCitra').Value:=NamaFileBmp;
qInsert.Parameters.ParamByName('inputpoint').Value:=seInput.Value;
qInsert.Parameters.ParamByName('ambangkontras').Value:=SeKontrass.Value;
qInsert.Parameters.ParamByName('luasminimal').Value:=SeMinPixels.Value;
qInsert.Parameters.ParamByName('mer').Value:=SeAmer.Value;
qInsert.Parameters.ParamByName('ao').Value:=IcalImage.arrayKordinatImage[listbox1.ItemIndex].Ao;
qInsert.Parameters.ParamByName('ratio').Value:=IcalImage.arrayKordinatImage[listbox1.ItemIndex].R;
qInsert.Parameters.ParamByName('edgedensity').Value:=IcalImage.arrayKordinatImage[listbox1.ItemIndex].Ed;
qInsert.Parameters.ParamByName('tanggalinput').Value:=now;

jpeg:=TJPEGImage.Create;
jpeg.Assign(chasil.Picture.Bitmap);
jpeg.SaveToFile(ExtractFilePath(application.ExeName)+'\training\plat\'+NamaFileBmp);
jpeg.Assign(cawal.Picture.Bitmap);
jpeg.SaveToFile(ExtractFilePath(application.ExeName)+'\training\'+NamaFileBmp);

jpeg.Free;

QInsert.ExecSQL;
TTraining.Close;
nomor1:=0;
nomor2:=0;
TTraining.Open;
showmessage('data telah diinputkan sebagai training');

end;

function GetTempDirectory: String;
var
  tempFolder: array[0..MAX_PATH] of Char;
begin
  GetTempPath(MAX_PATH, @tempFolder);
  result := StrPas(tempFolder);
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
var FileIni:TInifile;
begin
fileIni:=TIniFile.Create(GetTempDirectory+'\ical.INI');
  try
    FileIni.WriteInteger('Params','nInput',seInput.Value);
    FileIni.WriteInteger('Params','Kontrass',SeKontrass.Value);
    FileIni.WriteInteger('Params','MinPixels',SeMinPixels.Value);
    FileIni.WriteInteger('Params','Amer',SeAmer.Value);
    FileIni.WriteInteger('Params','SeMin2',SeMin2.Value);
    FileIni.WriteInteger('Params','kontrass2',Sekontrass2.Value);

    FileIni.WriteString('OperatorX','x00',SGopX.Cells[0,0]);
    FileIni.WriteString('OperatorX','x01',SGopX.Cells[0,1]);
    FileIni.WriteString('OperatorX','x02',SGopX.Cells[0,2]);
    FileIni.WriteString('OperatorX','x10',SGopX.Cells[1,0]);
    FileIni.WriteString('OperatorX','x11',SGopX.Cells[1,1]);
    FileIni.WriteString('OperatorX','x12',SGopX.Cells[1,2]);
    FileIni.WriteString('OperatorX','x20',SGopX.Cells[2,0]);
    FileIni.WriteString('OperatorX','x21',SGopX.Cells[2,1]);
    FileIni.WriteString('OperatorX','x22',SGopX.Cells[2,2]);

    FileIni.WriteString('Operatory','y00',SGopy.Cells[0,0]);
    FileIni.WriteString('Operatory','y01',SGopy.Cells[0,1]);
    FileIni.WriteString('Operatory','y02',SGopy.Cells[0,2]);
    FileIni.WriteString('Operatory','y10',SGopy.Cells[1,0]);
    FileIni.WriteString('Operatory','y11',SGopy.Cells[1,1]);
    FileIni.WriteString('Operatory','y12',SGopy.Cells[1,2]);
    FileIni.WriteString('Operatory','y20',SGopy.Cells[2,0]);
    FileIni.WriteString('Operatory','y21',SGopy.Cells[2,1]);
    FileIni.WriteString('Operatory','y22',SGopy.Cells[2,2]);


    FileIni.WriteString('Database','NamaFile',namaDb);
  finally
    fileIni.Free;
  end;
end;

procedure TForm1.ListBox1KeyPress(Sender: TObject; var Key: Char);
begin
if Key=#13 then  Jadikandatatraining1Click(self);
end;


procedure TForm1.loadDbFiles;
begin
if TTraining['NamaCitra']=null then exit;
if FileExists(pathExe+'training\'+ TTraining['NamaCitra']) then
    CTraining.Picture.LoadFromFile(pathExe+'training\'+ TTraining['NamaCitra']);
if FileExists(pathExe+'training\plat\'+ TTraining['NamaCitra']) then
    CPlat.Picture.LoadFromFile(pathExe+'training\plat\'+ TTraining['NamaCitra']);

end;

procedure TForm1.DBNavigator1Click(Sender: TObject; Button: TNavigateBtn);
begin
loadDbFiles;
end;

procedure TForm1.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
if TTraining['nomor']mod 2 =0 then
  DBGrid1.Canvas.Brush.Color:=clteal
else DBGrid1.Canvas.Brush.Color:=clSkyBlue;

DBGrid1.DefaultDrawColumnCell
  (Rect, DataCol, Column, State);

end;

function resizeBMP(gbr:tbitmap;w:integer):tbitmap;
var i,j:integer;
row1, row2 : PRGBTripleArray;
facW, facH, ratio : real;
begin
ratio :=   (gbr.Width/w);
facW:=gbr.Width / ratio;
facH:=gbr.Height / ratio;

result:=TBitmap.Create;
result.Width:=round(facW);
result.Height:=round(facH);
result.PixelFormat:=pf24bit;

 for i:=0 to result.Height-1 do
 begin

 row1:=gbr.ScanLine[trunc(i*ratio)];

 row2:=Result.ScanLine[i];
 
    for j:=0 to Result.Width-1 do
      begin
          row2[j].rgbtBlue:=row1[trunc(j*ratio)].rgbtBlue;
          row2[j].rgbtGreen:=row1[trunc(j*ratio)].rgbtGreen;
          row2[j].rgbtRed:=row1[trunc(j*ratio)].rgbtRed;
      end;
 end;
end;


procedure TForm1.FileListBox1Change(Sender: TObject);
var
bmp,bmp2:TBitmap;
jpeg:TJPEGImage;
//tempImage:Timage;
thumbRect : TRect;

begin
OpenDialog1.FileName:=FileListBox1.FileName;
  bmp:=tbitmap.Create;
    if uppercase(ExtractFileExt(opendialog1.FileName))='.JPG' then
      begin
      jpeg:=TJPEGImage.Create;
      jpeg.LoadFromFile(OpenDialog1.FileName);
      bmp.Assign(jpeg);
      jpeg.Free;
      end
    else if uppercase(ExtractFileExt(opendialog1.FileName))='.BMP' then
      bmp.LoadFromFile(OpenDialog1.FileName);

      statusbar1.Panels.Items[0].Text:=inttostr(bmp.Width)+'x'+inttostr(bmp.Height);

      if bmp.Width > 400 then
        begin
        bmp2:=TBitmap.Create;
        bmp2.Width:= bmp.Width * 400 div bmp.Width;
        bmp2.Height:= bmp.Height * 400 div bmp.Width;
        bmp2.PixelFormat:=pf24bit;

        thumbRect.Left:=0;
        thumbRect.Top:=0;
        thumbRect.Right:= bmp2.Width;
        thumbRect.Bottom:=bmp2.Height;

        bmp2.Canvas.StretchDraw(thumbrect,bmp);

        bmp:=bmp2;
        //showmessage(inttostr(bmp.Width));
        end;
      Cbiner.Picture.Bitmap:=bmp;
      cawal.Picture.Bitmap:=bmp;
      csegment.Picture.Bitmap:=bmp;
      cHasil.Picture.Bitmap:=bmp;
      Clokal.Picture.Bitmap:=bmp;
      ctepi.Picture.Bitmap:=bmp;
      CFoundedPlat.Picture.Bitmap:=bmp;
      bmp.Free;
      panel1.show;
      NamaFileBmp:=ExtractFileName(opendialog1.FileName);
      Form1.Caption:= NamaFileBmp;
      PageControl1.TabIndex:=0;
end;

procedure TForm1.TabSheet1Enter(Sender: TObject);
begin
FileListBox1.SetFocus;
end;

procedure TForm1.Sekontrass2Change(Sender: TObject);
begin
if  (SeKontrass2.Value=null) then  exit;
PageControl1.TabIndex:=6;
CFoundedPlat.Picture.Bitmap:=icalimage.binarisasi(chasil.Picture.Bitmap,Sekontrass2.Value);
end;

procedure TForm1.btCharClick(Sender: TObject);
var
i,j:integer;
ketemu,simpan:boolean;
jpeg:TJPEGImage;
bmp:array[0..10]of tbitmap;
//img:array[0..7]timage;

begin
IcalImage.lokalisasi(0,SeMin2.Value,CFoundedPlat);
PageControl1.TabIndex:=7;

label14.Caption :=(inttostr(IcalImage.jumArrayKI));
lbchar.Items.Clear;
for  i:=0 to IcalImage.jumArrayKI-1 do
  begin
     icalImage.PanalisaBentuk(CFoundedPlat,clblue,SeAmer.value,i);
     LBChar.Items.Add('C'+intTostr(i+1)
     +' |R: ' + format('%5.2f',[icalImage.arrayKordinatImage[i].R])
     +' |Ao: '+ format('%6.2f',[icalImage.arrayKordinatImage[i].Ao])
     +' |Ed: '+ format('%8.2f',[icalImage.arrayKordinatImage[i].Ed])
     );
  end;



j:=0;
for i:=0 to 10 do
  begin
  bmp[i]:=tbitmap.Create;
       bmp[j].PixelFormat:=pf24bit;
       bmp[j].Width:=1;
       bmp[j].Height:=1;
       
  end;

for i:=0 to LBChar.Items.Count-1 do
    begin
     if (icalimage.arrayKordinatImage[i].R < 1) and
     (icalimage.arrayKordinatImage[i].R > 0.07) then
       begin
        bmp[j]:=icalImage.analisaBentuk(chasil,clblue,9,i);
        inc(j);
       end;
    end;

cchar0.Picture.Bitmap:= bmp[0];
cchar1.Picture.Bitmap:= bmp[1];
cchar2.Picture.Bitmap:= bmp[2];
cchar3.Picture.Bitmap:= bmp[3];
cchar4.Picture.Bitmap:= bmp[4];
cchar5.Picture.Bitmap:= bmp[5];
cchar6.Picture.Bitmap:= bmp[6];
cchar7.Picture.Bitmap:= bmp[7];


ketemu:=false;
simpan:=false;
if (MessageDlg('Hasil sesuai?', mtConfirmation,[mbYes,mbNo],0)=mrYes)then
  ketemu:=true;
if (MessageDlg('Simpan Percobaan?', mtConfirmation,[mbYes,mbNo],0)=mrYes)then
  simpan:=true;

if simpan then
    begin
      QsimpanHasil.Close;
      QSimpanHasil.Parameters.ParamByName('benar').Value:=ketemu;
      QSimpanHasil.Parameters.ParamByName('namaCitra').Value:=NamaFileBmp;
      QSimpanHasil.Parameters.ParamByName('inputpoint').Value:=seInput.Value;
      QSimpanHasil.Parameters.ParamByName('ambangkontras').Value:=SeKontrass.Value;
      QSimpanHasil.Parameters.ParamByName('luasminimal').Value:=SeMinPixels.Value;
      QSimpanHasil.Parameters.ParamByName('mer').Value:=SeAmer.Value;
      QSimpanHasil.Parameters.ParamByName('ao').Value:=IcalImage.Ao;
      QSimpanHasil.Parameters.ParamByName('ratio').Value:=IcalImage.R;
      QSimpanHasil.Parameters.ParamByName('edgedensity').Value:=IcalImage.Ed;
      QSimpanHasil.Parameters.ParamByName('tanggalinput').Value:=now;
      QsimpanHasil.ExecSQL;
      TDataUji.close;
    TDataUji.open;
    jpeg:=TJPEGImage.Create;
    jpeg.Assign(cawal.Picture.Bitmap);
    jpeg.SaveToFile(ExtractFilePath(application.ExeName)+'\uji\'+NamaFileBmp);
    //jpeg.Free;
    end;


end;

procedure TForm1.LBCharClick(Sender: TObject);
begin
     CChar0.Picture.Bitmap:=icalImage.analisaBentuk(chasil,clblue,9,LBChar.ItemIndex);
     //CChar.Picture.Bitmap:=invert(CChar.Picture.Bitmap);
end;

procedure TForm1.insertasnonplate1Click(Sender: TObject);
begin
QNonPlate.Close;
//(,,,,,,,)
QNonPlate.Parameters.ParamByName('namaCitra').Value:=NamaFileBmp;
QNonPlate.Parameters.ParamByName('inputpoint').Value:=seInput.Value;
QNonPlate.Parameters.ParamByName('ambangkontras').Value:=SeKontrass.Value;
QNonPlate.Parameters.ParamByName('luasminimal').Value:=SeMinPixels.Value;
QNonPlate.Parameters.ParamByName('mer').Value:=SeAmer.Value;
QNonPlate.Parameters.ParamByName('ao').Value:=IcalImage.arrayKordinatImage[listbox1.ItemIndex].Ao;
QNonPlate.Parameters.ParamByName('ratio').Value:=IcalImage.arrayKordinatImage[listbox1.ItemIndex].R;
QNonPlate.Parameters.ParamByName('edgedensity').Value:=IcalImage.arrayKordinatImage[listbox1.ItemIndex].Ed;
QNonPlate.Parameters.ParamByName('tanggalinput').Value:=now;


QNonPlate.ExecSQL;

showmessage('Non Plat inserted');

end;

procedure TForm1.SeKontrassChange(Sender: TObject);
begin
if (SeKontrass.Value=null) then     exit;
      PageControl1.TabIndex:=3;
      Cbiner.Picture.Bitmap:=icalimage.binarisasi(Ctepi.Picture.Bitmap,SeKontrass.Value);
end;

procedure TForm1.TTrainingCalcFields(DataSet: TDataSet);
begin
inc(nomor1);
TTraining['nomor']:=nomor1;
end;

procedure TForm1.DBGrid3DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
if TDataUji['benar'] then
  DBGrid3.Canvas.Brush.Color:=clteal
else DBGrid3.Canvas.Brush.Color:=clSkyBlue;

DBGrid3.DefaultDrawColumnCell
  (Rect, DataCol, Column, State);

end;

procedure TForm1.DBGrid1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  pt: TGridcoord;
begin
  pt:= DBGrid1.MouseCoord(x, y);

  if pt.y=0 then
    DBGrid1.Cursor:=crHandPoint
  else
    DBGrid1.Cursor:=crDefault;
end;



procedure TForm1.DBGrid1TitleClick(Column: TColumn);
{$J+}
 const PreviousColumnIndex : integer = 0;
{$J-}
begin
  if DBGrid1.DataSource.DataSet is TCustomADODataSet then
  with TCustomADODataSet(DBGrid1.DataSource.DataSet) do
  begin
    try
      DBGrid1.Columns[PreviousColumnIndex].title.Font.Style :=
      DBGrid1.Columns[PreviousColumnIndex].title.Font.Style - [fsBold];
    except
    end;

    Column.title.Font.Style := 
    Column.title.Font.Style + [fsBold];
    PreviousColumnIndex := Column.Index;

    if (Pos(Column.Field.FieldName, Sort) = 1)
    and (Pos(' DESC', Sort)= 0) then
      Sort := Column.Field.FieldName + ' DESC'
    else
      Sort := Column.Field.FieldName + ' ASC';
  end;
end;


procedure TForm1.DBGrid3TitleClick(Column: TColumn);
{$J+}
 const PreviousColumnIndex : integer = 0;
{$J-}
begin
  if DBGrid3.DataSource.DataSet is TCustomADODataSet then
  with TCustomADODataSet(DBGrid3.DataSource.DataSet) do
  begin
    try
      DBGrid3.Columns[PreviousColumnIndex].title.Font.Style :=
      DBGrid3.Columns[PreviousColumnIndex].title.Font.Style - [fsBold];
    except
    end;

    Column.title.Font.Style :=
    Column.title.Font.Style + [fsBold];
    PreviousColumnIndex := Column.Index;

    if (Pos(Column.Field.FieldName, Sort) = 1)
    and (Pos(' DESC', Sort)= 0) then
      Sort := Column.Field.FieldName + ' DESC'
    else
      Sort := Column.Field.FieldName + ' ASC';
  end;
end; 

procedure TForm1.Refresh1Click(Sender: TObject);
begin
FileListBox1.Refresh;
end;

procedure TForm1.Refresh2Click(Sender: TObject);
begin
DirectoryListBox1.Refresh;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  CSegment.Picture.Bitmap:=IcalImage.msa(5,seInput.Value,SERadius.Value,Cawal);
  PageControl1.TabIndex:=1;

end;

procedure TForm1.TDataUjiAfterDelete(DataSet: TDataSet);
begin
if DeleteFile(deletecitra) then
   showmessage('File deleted');
end;

procedure TForm1.DataSource3DataChange(Sender: TObject; Field: TField);
begin
image3.Picture.Bitmap:=Tbitmap.Create;
if TDataUji['NamaCitra']=null then
    begin
    image3.Picture.Bitmap.Free;
    exit;
    end;
    OLDNAMACITRA:=NAMACITRA;
    namacitra:=pathExe+'uji\'+ TDataUji['NamaCitra'];
if FileExists(NamaCitra) then
    image3.Picture.LoadFromFile(namacitra);



end;

procedure TForm1.DataSource1DataChange(Sender: TObject; Field: TField);
begin
loadDbFiles;

end;

procedure TForm1.DataSource3UpdateData(Sender: TObject);
begin
RenameFile(OLDnamacitra,pathExe+'uji\'+ TDataUji['NamaCitra']);

end;

procedure TForm1.TDataUjiBeforeDelete(DataSet: TDataSet);
begin
deletecitra:=namacitra;
end;

end.

