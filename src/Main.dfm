object Form1: TForm1
  Left = 146
  Top = 45
  Width = 751
  Height = 513
  Caption = 'Image Clustering and Localization::::.....'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 114
    Width = 743
    Height = 345
    ActivePage = TabSheet10
    Align = alClient
    TabOrder = 0
    OnChange = PageControl1Change
    object TabSheet1: TTabSheet
      Caption = 'Citra Awal'
      OnEnter = TabSheet1Enter
      object Panel4: TPanel
        Left = 560
        Top = 0
        Width = 175
        Height = 317
        Align = alRight
        Caption = 'Panel4'
        TabOrder = 0
        object DriveComboBox1: TDriveComboBox
          Left = 14
          Top = 288
          Width = 145
          Height = 19
          DirList = DirectoryListBox1
          TabOrder = 2
        end
        object DirectoryListBox1: TDirectoryListBox
          Left = 14
          Top = 184
          Width = 145
          Height = 97
          FileList = FileListBox1
          ItemHeight = 16
          PopupMenu = PopupMenu4
          TabOrder = 1
        end
        object FileListBox1: TFileListBox
          Left = 14
          Top = 24
          Width = 145
          Height = 153
          ItemHeight = 13
          Mask = '*.bmp;*.jpg'
          PopupMenu = PopupMenu3
          TabOrder = 0
          OnChange = FileListBox1Change
        end
      end
      object Panel9: TPanel
        Left = 0
        Top = 0
        Width = 560
        Height = 317
        Align = alClient
        Caption = 'Panel9'
        TabOrder = 1
        object Cawal: TImage
          Left = 10
          Top = 10
          Width = 322
          Height = 240
          AutoSize = True
          PopupMenu = PopupMenu1
          Proportional = True
        end
        object StatusBar1: TStatusBar
          Left = 1
          Top = 297
          Width = 558
          Height = 19
          Panels = <
            item
              Width = 50
            end>
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Segmentasi'
      ImageIndex = 1
      object CSegment: TImage
        Left = 10
        Top = 10
        Width = 400
        Height = 266
        AutoSize = True
        PopupMenu = PopupMenu1
        Proportional = True
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Deteksi Tepi'
      ImageIndex = 3
      object Ctepi: TImage
        Left = 10
        Top = 10
        Width = 400
        Height = 266
        AutoSize = True
        PopupMenu = PopupMenu1
        Proportional = True
      end
      object Panel10: TPanel
        Left = 536
        Top = 0
        Width = 199
        Height = 317
        Align = alRight
        Caption = 'Panel10'
        TabOrder = 0
        object Label4: TLabel
          Left = 32
          Top = 8
          Width = 51
          Height = 13
          Caption = 'Operator X'
        end
        object Label5: TLabel
          Left = 32
          Top = 128
          Width = 51
          Height = 13
          Caption = 'Operator Y'
        end
        object SGopX: TStringGrid
          Left = 24
          Top = 24
          Width = 137
          Height = 89
          ColCount = 3
          DefaultColWidth = 40
          FixedCols = 0
          RowCount = 3
          FixedRows = 0
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
          TabOrder = 0
        end
        object sgopy: TStringGrid
          Left = 24
          Top = 144
          Width = 137
          Height = 89
          ColCount = 3
          DefaultColWidth = 40
          FixedCols = 0
          RowCount = 3
          FixedRows = 0
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
          TabOrder = 1
        end
      end
    end
    object TabSheet9: TTabSheet
      Caption = 'Binarisasi'
      ImageIndex = 8
      object Cbiner: TImage
        Left = 10
        Top = 10
        Width = 400
        Height = 266
        AutoSize = True
        PopupMenu = PopupMenu1
        Proportional = True
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'Lokalisasi'
      ImageIndex = 5
      object Clokal: TImage
        Left = 10
        Top = 10
        Width = 400
        Height = 266
        AutoSize = True
        PopupMenu = PopupMenu1
        Proportional = True
      end
    end
    object TabSheet7: TTabSheet
      Caption = 'Analisa'
      ImageIndex = 6
      object Chasil: TImage
        Left = 10
        Top = 10
        Width = 400
        Height = 266
        AutoSize = True
        Center = True
        PopupMenu = PopupMenu1
        Proportional = True
      end
      object ListBox1: TListBox
        Left = 456
        Top = 0
        Width = 279
        Height = 317
        Align = alRight
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ItemHeight = 14
        ParentFont = False
        PopupMenu = PopupMenu2
        TabOrder = 0
        OnClick = ListBox1Click
        OnKeyPress = ListBox1KeyPress
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Image Plat Nomor'
      ImageIndex = 5
      object DBGrid2: TDBGrid
        Left = 0
        Top = 184
        Width = 735
        Height = 133
        Align = alBottom
        DataSource = DataSource2
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'NamaKelas'
            Width = 86
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ao'
            Width = 97
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'C_Ao'
            Width = 85
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'R'
            Width = 99
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'C_R'
            Width = 102
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ed'
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CEd'
            Width = 85
            Visible = True
          end>
      end
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 695
        Height = 184
        Align = alCustom
        TabOrder = 1
        object Image2: TImage
          Left = 1
          Top = 1
          Width = 77
          Height = 73
          Align = alClient
          AutoSize = True
          Center = True
          Picture.Data = {
            0A544A504547496D616765D7020000FFD8FFE000104A46494600010101006000
            600000FFDB004300080606070605080707070909080A0C140D0C0B0B0C191213
            0F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F2739
            3D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232
            3232323232323232323232323232323232323232323232323232323232323232
            32323232323232323232323232FFC00011080049004D03012200021101031101
            FFC4001F0000010501010101010100000000000000000102030405060708090A
            0BFFC400B5100002010303020403050504040000017D01020300041105122131
            410613516107227114328191A1082342B1C11552D1F02433627282090A161718
            191A25262728292A3435363738393A434445464748494A535455565758595A63
            6465666768696A737475767778797A838485868788898A92939495969798999A
            A2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6
            D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F01000301
            01010101010101010000000000000102030405060708090A0BFFC400B5110002
            0102040403040705040400010277000102031104052131061241510761711322
            328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728
            292A35363738393A434445464748494A535455565758595A636465666768696A
            737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7
            A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3
            E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00F9FE
            8A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A002
            8A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A002
            8A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A002
            8A28A00FFFD9}
          PopupMenu = PopupMenu1
          Stretch = True
        end
        object CFoundedPlat: TImage
          Left = 8
          Top = 10
          Width = 145
          Height = 71
          AutoSize = True
          Center = True
          PopupMenu = PopupMenu1
          Proportional = True
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'Lokalisasi Karakter'
      ImageIndex = 6
      object LBChar: TListBox
        Left = 456
        Top = 0
        Width = 279
        Height = 317
        Align = alRight
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ItemHeight = 14
        ParentFont = False
        PopupMenu = PopupMenu2
        TabOrder = 0
        OnClick = LBCharClick
      end
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 456
        Height = 317
        Align = alClient
        TabOrder = 1
        object Image1: TImage
          Left = 1
          Top = 1
          Width = 77
          Height = 73
          Align = alClient
          AutoSize = True
          Center = True
          Picture.Data = {
            0A544A504547496D616765D7020000FFD8FFE000104A46494600010101006000
            600000FFDB004300080606070605080707070909080A0C140D0C0B0B0C191213
            0F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F2739
            3D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232
            3232323232323232323232323232323232323232323232323232323232323232
            32323232323232323232323232FFC00011080049004D03012200021101031101
            FFC4001F0000010501010101010100000000000000000102030405060708090A
            0BFFC400B5100002010303020403050504040000017D01020300041105122131
            410613516107227114328191A1082342B1C11552D1F02433627282090A161718
            191A25262728292A3435363738393A434445464748494A535455565758595A63
            6465666768696A737475767778797A838485868788898A92939495969798999A
            A2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6
            D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F01000301
            01010101010101010000000000000102030405060708090A0BFFC400B5110002
            0102040403040705040400010277000102031104052131061241510761711322
            328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728
            292A35363738393A434445464748494A535455565758595A636465666768696A
            737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7
            A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3
            E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00F9FE
            8A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A002
            8A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A002
            8A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A002
            8A28A00FFFD9}
          PopupMenu = PopupMenu1
          Stretch = True
        end
        object CChar0: TImage
          Left = 15
          Top = 18
          Width = 42
          Height = 39
          AutoSize = True
          Center = True
          PopupMenu = PopupMenu1
          Proportional = True
        end
        object CChar1: TImage
          Left = 63
          Top = 18
          Width = 42
          Height = 39
          AutoSize = True
          Center = True
          PopupMenu = PopupMenu1
          Proportional = True
        end
        object CChar2: TImage
          Left = 111
          Top = 18
          Width = 42
          Height = 39
          AutoSize = True
          Center = True
          PopupMenu = PopupMenu1
          Proportional = True
        end
        object CChar3: TImage
          Left = 159
          Top = 18
          Width = 42
          Height = 39
          AutoSize = True
          Center = True
          PopupMenu = PopupMenu1
          Proportional = True
        end
        object CChar4: TImage
          Left = 207
          Top = 18
          Width = 42
          Height = 39
          AutoSize = True
          Center = True
          PopupMenu = PopupMenu1
          Proportional = True
        end
        object CChar5: TImage
          Left = 255
          Top = 18
          Width = 42
          Height = 39
          AutoSize = True
          Center = True
          PopupMenu = PopupMenu1
          Proportional = True
        end
        object CChar6: TImage
          Left = 303
          Top = 18
          Width = 42
          Height = 39
          AutoSize = True
          Center = True
          PopupMenu = PopupMenu1
          Proportional = True
        end
        object CChar7: TImage
          Left = 351
          Top = 18
          Width = 42
          Height = 39
          AutoSize = True
          Center = True
          PopupMenu = PopupMenu1
          Proportional = True
        end
      end
    end
    object TabSheet8: TTabSheet
      Caption = 'Data Training'
      ImageIndex = 7
      object Panel2: TPanel
        Left = 335
        Top = 0
        Width = 400
        Height = 317
        Align = alRight
        Caption = 'Panel2'
        TabOrder = 0
        object CTraining: TImage
          Left = 24
          Top = 10
          Width = 353
          Height = 263
          PopupMenu = PopupMenu1
          Proportional = True
        end
        object Cplat: TImage
          Left = 24
          Top = 275
          Width = 105
          Height = 38
          PopupMenu = PopupMenu1
          Proportional = True
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 335
        Height = 317
        Align = alClient
        Caption = 'Panel2'
        TabOrder = 1
        object DBGrid1: TDBGrid
          Left = 1
          Top = 41
          Width = 333
          Height = 275
          Align = alClient
          DataSource = DataSource1
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = DBGrid1DrawColumnCell
          OnMouseMove = DBGrid1MouseMove
          OnTitleClick = DBGrid1TitleClick
          Columns = <
            item
              Expanded = False
              FieldName = 'NamaCitra'
              Width = 121
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ao'
              Width = 34
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ratio'
              Width = 37
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EdgeDensity'
              Title.Caption = 'Edge'
              Width = 67
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'InputPoint'
              Title.Caption = 'nInput'
              Width = 37
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AmbangKontras'
              Title.Caption = 'Kontras'
              Width = 41
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LuasMinimal'
              Title.Caption = 'MinLuas'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MER'
              Width = 41
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'tanggalinput'
              Visible = True
            end>
        end
        object DBNavigator1: TDBNavigator
          Left = 1
          Top = 1
          Width = 333
          Height = 40
          DataSource = DataSource1
          Align = alTop
          TabOrder = 1
          OnClick = DBNavigator1Click
        end
      end
    end
    object TabSheet10: TTabSheet
      Caption = 'Data Uji'
      ImageIndex = 9
      object Panel7: TPanel
        Left = 335
        Top = 0
        Width = 400
        Height = 317
        Align = alRight
        Caption = 'Panel2'
        TabOrder = 0
        object Image3: TImage
          Left = 24
          Top = 10
          Width = 353
          Height = 263
          PopupMenu = PopupMenu1
          Proportional = True
        end
      end
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 335
        Height = 317
        Align = alClient
        Caption = 'Panel2'
        TabOrder = 1
        object DBGrid3: TDBGrid
          Left = 1
          Top = 33
          Width = 333
          Height = 283
          Align = alClient
          DataSource = DataSource3
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = DBGrid3DrawColumnCell
          OnTitleClick = DBGrid3TitleClick
          Columns = <
            item
              Expanded = False
              FieldName = 'NamaCitra'
              Width = 121
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ao'
              Width = 34
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ratio'
              Width = 37
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EdgeDensity'
              Title.Caption = 'Edge'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'tanggalinput'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'InputPoint'
              Title.Caption = 'nInput'
              Width = 37
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AmbangKontras'
              Title.Caption = 'Kontras'
              Width = 41
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LuasMinimal'
              Title.Caption = 'MinLuas'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MER'
              Width = 41
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'benar'
              Visible = True
            end>
        end
        object DBNavigator2: TDBNavigator
          Left = 1
          Top = 1
          Width = 333
          Height = 32
          DataSource = DataSource3
          Align = alTop
          TabOrder = 1
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 743
    Height = 114
    Align = alTop
    TabOrder = 1
    Visible = False
    object Label1: TLabel
      Left = 358
      Top = 10
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'MER'
    end
    object Label2: TLabel
      Left = 138
      Top = 10
      Width = 28
      Height = 13
      Alignment = taRightJustify
      Caption = 'Iterasi'
    end
    object Label3: TLabel
      Left = 266
      Top = 10
      Width = 60
      Height = 13
      Alignment = taRightJustify
      Caption = 'Luas minimal'
    end
    object Label14: TLabel
      Left = 676
      Top = 36
      Width = 6
      Height = 13
      Alignment = taRightJustify
      Caption = '0'
    end
    object Label15: TLabel
      Left = 176
      Top = 10
      Width = 78
      Height = 13
      Alignment = taRightJustify
      Caption = 'Ambang Kontras'
    end
    object Label16: TLabel
      Left = 600
      Top = 36
      Width = 64
      Height = 13
      Caption = 'Terlokalisasi :'
    end
    object Label8: TLabel
      Left = 421
      Top = 10
      Width = 49
      Height = 13
      Alignment = taRightJustify
      Caption = 'Brightness'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object Label6: TLabel
      Left = 482
      Top = 10
      Width = 60
      Height = 13
      Alignment = taRightJustify
      Caption = 'Luas minimal'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object BtSegmen: TButton
      Left = 95
      Top = 56
      Width = 75
      Height = 25
      Caption = 'Segmentasi'
      TabOrder = 0
      OnClick = BtSegmenClick
    end
    object BtLokalisasi: TButton
      Left = 255
      Top = 56
      Width = 75
      Height = 25
      Caption = 'Lokalisasi'
      TabOrder = 2
      OnClick = BtLokalisasiClick
    end
    object BtAnalisa: TButton
      Left = 335
      Top = 56
      Width = 75
      Height = 25
      Caption = 'Analisa'
      TabOrder = 3
      OnClick = BtAnalisaClick
    end
    object seInput: TSpinEdit
      Left = 120
      Top = 31
      Width = 49
      Height = 22
      TabStop = False
      MaxValue = 25
      MinValue = 0
      TabOrder = 4
      Value = 5
    end
    object SeAmer: TSpinEdit
      Left = 336
      Top = 31
      Width = 49
      Height = 22
      TabStop = False
      Increment = 100
      MaxValue = 10000
      MinValue = 0
      TabOrder = 5
      Value = 100
    end
    object btEdgeDet: TButton
      Left = 176
      Top = 56
      Width = 75
      Height = 25
      Caption = 'Deteksi Tepi'
      TabOrder = 1
      OnClick = btEdgeDetClick
    end
    object SeMinPixels: TSpinEdit
      Left = 272
      Top = 31
      Width = 57
      Height = 22
      TabStop = False
      Increment = 25
      MaxValue = 10000
      MinValue = 0
      TabOrder = 6
      Value = 175
    end
    object btReset: TButton
      Left = 576
      Top = 56
      Width = 107
      Height = 25
      Caption = 'Reset'
      TabOrder = 7
      OnClick = btResetClick
    end
    object SeKontrass: TSpinEdit
      Left = 200
      Top = 31
      Width = 49
      Height = 22
      TabStop = False
      Increment = 5
      MaxValue = 255
      MinValue = 0
      TabOrder = 8
      Value = 65
      OnChange = SeKontrassChange
    end
    object SERadius: TSpinEdit
      Left = 120
      Top = 87
      Width = 49
      Height = 22
      TabStop = False
      Increment = 100
      MaxValue = 10000
      MinValue = 0
      TabOrder = 9
      Value = 1000
    end
    object Sekontrass2: TSpinEdit
      Left = 424
      Top = 31
      Width = 49
      Height = 22
      TabStop = False
      Increment = 5
      MaxValue = 765
      MinValue = 0
      TabOrder = 10
      Value = 165
      OnChange = Sekontrass2Change
    end
    object seMin2: TSpinEdit
      Left = 488
      Top = 31
      Width = 57
      Height = 22
      TabStop = False
      Increment = 10
      MaxValue = 10000
      MinValue = 0
      TabOrder = 11
      Value = 25
    end
    object btChar: TButton
      Left = 416
      Top = 56
      Width = 75
      Height = 25
      Caption = 'Get Character'
      TabOrder = 12
      OnClick = btCharClick
    end
    object Button1: TButton
      Left = 16
      Top = 56
      Width = 75
      Height = 25
      Caption = 'Random input'
      TabOrder = 13
      OnClick = Button1Click
    end
  end
  object MainMenu1: TMainMenu
    Left = 444
    Top = 144
    object File1: TMenuItem
      Caption = 'File'
      object exit1: TMenuItem
        Caption = 'E&xit'
        OnClick = exit1Click
      end
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 476
    Top = 208
    object SimpanGambar1: TMenuItem
      Caption = 'Simpan Gambar'
      OnClick = SimpanGambar1Click
    end
  end
  object PopupMenu2: TPopupMenu
    Left = 508
    Top = 144
    object Jadikandatatraining1: TMenuItem
      Caption = 'Insert as data training'
      OnClick = Jadikandatatraining1Click
    end
    object insertasnonplate1: TMenuItem
      Caption = 'insert as non plate'
      OnClick = insertasnonplate1Click
    end
  end
  object DataSource1: TDataSource
    DataSet = TTraining
    OnDataChange = DataSource1DataChange
    Left = 476
    Top = 176
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=db.mdb;Persist Secu' +
      'rity Info=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 444
    Top = 176
  end
  object TTraining: TADOTable
    Connection = ADOConnection1
    CursorType = ctStatic
    OnCalcFields = TTrainingCalcFields
    TableName = 'dataLatih'
    Left = 508
    Top = 176
    object TTrainingNamaCitra: TWideStringField
      FieldName = 'NamaCitra'
      Size = 255
    end
    object TTrainingAo: TBCDField
      FieldName = 'Ao'
      Precision = 9
      Size = 0
    end
    object TTrainingRatio: TBCDField
      FieldName = 'Ratio'
      Precision = 9
      Size = 0
    end
    object TTrainingEdgeDensity: TBCDField
      FieldName = 'EdgeDensity'
      Precision = 9
      Size = 0
    end
    object TTrainingInputPoint: TIntegerField
      FieldName = 'InputPoint'
    end
    object TTrainingAmbangKontras: TIntegerField
      FieldName = 'AmbangKontras'
    end
    object TTrainingLuasMinimal: TIntegerField
      FieldName = 'LuasMinimal'
    end
    object TTrainingMER: TIntegerField
      FieldName = 'MER'
    end
    object TTrainingtanggalinput: TDateTimeField
      FieldName = 'tanggalinput'
    end
    object TTrainingnomor: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'nomor'
      Calculated = True
    end
  end
  object QInsert: TADOQuery
    Connection = ADOConnection1
    Parameters = <
      item
        Name = 'namaCitra'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'inputpoint'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'ambangkontras'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'luasminimal'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'mer'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'ao'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'ratio'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'edgedensity'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'tanggalinput'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      
        'insert into dataLatih (namaCitra,inputpoint,ambangkontras,luasmi' +
        'nimal,mer,ao,ratio,edgedensity,tanggalinput)'
      
        'values (:namaCitra, :inputpoint, :ambangkontras, :luasminimal, :' +
        'mer, :ao, :ratio, :edgedensity,:tanggalinput)')
    Left = 540
    Top = 176
  end
  object QNonPlate: TADOQuery
    Connection = ADOConnection1
    Parameters = <
      item
        Name = 'namaCitra'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'inputpoint'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'ambangkontras'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'luasminimal'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'mer'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'ao'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'ratio'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'edgedensity'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'tanggalinput'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      
        'insert into dataLatihNonPlat (namaCitra,inputpoint,ambangkontras' +
        ',luasminimal,mer,ao,ratio,edgedensity,tanggalinput)'
      
        'values (:namaCitra, :inputpoint, :ambangkontras, :luasminimal, :' +
        'mer, :ao, :ratio, :edgedensity,:tanggalinput)')
    Left = 540
    Top = 208
  end
  object TKlasifikasi: TADOTable
    Connection = ADOConnection1
    CursorType = ctStatic
    TableName = 'DataKlasifikasi'
    Left = 544
    Top = 144
    object TKlasifikasiNamaKelas: TWideStringField
      FieldName = 'NamaKelas'
      Size = 255
    end
    object TKlasifikasiAo: TFloatField
      FieldName = 'Ao'
    end
    object TKlasifikasiC_Ao: TFloatField
      FieldName = 'C_Ao'
    end
    object TKlasifikasiR: TFloatField
      FieldName = 'R'
    end
    object TKlasifikasiC_R: TFloatField
      FieldName = 'C_R'
    end
    object TKlasifikasiEd: TFloatField
      FieldName = 'Ed'
    end
    object TKlasifikasiCEd: TFloatField
      FieldName = 'CEd'
    end
  end
  object DataSource2: TDataSource
    DataSet = TKlasifikasi
    Left = 444
    Top = 210
  end
  object XPManifest1: TXPManifest
    Left = 444
    Top = 242
  end
  object QsimpanHasil: TADOQuery
    Connection = ADOConnection1
    Parameters = <
      item
        Name = 'benar'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'namaCitra'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'inputpoint'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'ambangkontras'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'luasminimal'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'mer'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'ao'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'ratio'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'edgedensity'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'tanggalinput'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      
        'insert into datauji (benar,namaCitra,inputpoint,ambangkontras,lu' +
        'asminimal,mer,ao,ratio,edgedensity,tanggalinput)'
      
        'values (:benar, :namaCitra, :inputpoint, :ambangkontras, :luasmi' +
        'nimal, :mer, :ao, :ratio, :edgedensity,:tanggalinput)')
    Left = 476
    Top = 240
  end
  object TDataUji: TADOTable
    Connection = ADOConnection1
    CursorType = ctStatic
    BeforeDelete = TDataUjiBeforeDelete
    AfterDelete = TDataUjiAfterDelete
    TableName = 'dataUji'
    Left = 508
    Top = 240
    object IntegerField1: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'nomor'
      Calculated = True
    end
    object WideStringField1: TWideStringField
      FieldName = 'NamaCitra'
      Size = 255
    end
    object IntegerField2: TIntegerField
      FieldName = 'InputPoint'
    end
    object IntegerField3: TIntegerField
      FieldName = 'AmbangKontras'
    end
    object IntegerField4: TIntegerField
      FieldName = 'LuasMinimal'
    end
    object IntegerField5: TIntegerField
      FieldName = 'MER'
    end
    object BCDField1: TBCDField
      FieldName = 'Ao'
      Precision = 9
      Size = 0
    end
    object BCDField2: TBCDField
      FieldName = 'Ratio'
      Precision = 9
      Size = 0
    end
    object BCDField3: TBCDField
      FieldName = 'EdgeDensity'
      Precision = 9
      Size = 0
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'tanggalinput'
    end
    object TDataUjibenar: TBooleanField
      FieldName = 'benar'
    end
  end
  object DataSource3: TDataSource
    DataSet = TDataUji
    OnDataChange = DataSource3DataChange
    OnUpdateData = DataSource3UpdateData
    Left = 540
    Top = 242
  end
  object PopupMenu3: TPopupMenu
    Left = 60
    Top = 210
    object Refresh1: TMenuItem
      Caption = 'Refresh'
      ShortCut = 116
      OnClick = Refresh1Click
    end
  end
  object PopupMenu4: TPopupMenu
    Left = 52
    Top = 354
    object Refresh2: TMenuItem
      Caption = 'Refresh'
      ShortCut = 117
      OnClick = Refresh2Click
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 483
    Top = 154
  end
  object SavePictureDialog1: TSaveDialog
    Left = 515
    Top = 210
  end
end
